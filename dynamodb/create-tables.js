'use strict';

const AWS = require( 'aws-sdk' );
const fs = require( 'fs' );

AWS.config.update({
	region: 'us-east-1',
	endpoint: 'http://localhost:8000'
});

var schemas = JSON.parse(fs.readFileSync('schemas/zerotrust-tables.json', 'utf-8'));
var client = new AWS.DynamoDB();
for (var i = 0; i < schemas.DatabaseTables.length; i++) {
	var tableSchema = schemas.DatabaseTables[i];
	client.createTable(tableSchema, function(error, success) {
		if (error) {
			console.error('Error creating table: ' + JSON.stringify(error) );
		}
		else {
			console.log('Created table: ' + JSON.stringify(success.TableDescription.TableName));
		}
	});
}
