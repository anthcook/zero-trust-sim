'use strict';

const AWS = require( 'aws-sdk' );

AWS.config.update({
	region: 'us-east-1',
	endpoint: 'http://localhost:8000'
});

var args = process.argv.slice(2);
if (args.length == 2) {
	var tableName = args[0];
	var itemKey = args[1];
	console.log('Querying item ' + itemKey + ' from table ' + tableName + '...');

	var docClient = new AWS.DynamoDB.DocumentClient();
	var tableKey = function(name) {
		if (name == 'CustomerDB') {
			return 'DeviceID';
		}
		else {
			return 'CustomerIdTimestampMetric';
		}
	};
	var params = {
		TableName: tableName,
		KeyConditionExpression: tableKey(tableName) + ' = :val',
		ExpressionAttributeValues: {
			':val': itemKey
		}
	};
	docClient.query( params, function(error, success) {
		if (error) {
			console.error( 'Query failed with error: ' + JSON.stringify(error) );
		}
		else {
			console.log( 'Result: ' + JSON.stringify(success) );
		}
	});
}
else {
	console.log( 'Must provide a table name and item key to query.' );
};
