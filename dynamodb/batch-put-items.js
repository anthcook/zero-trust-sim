'use strict';

const AWS = require( 'aws-sdk' );
const fs = require( 'fs' );

AWS.config.update({
	region: 'us-east-1',
	endpoint: 'http://localhost:8000'
});

var items = JSON.parse(fs.readFileSync('schemas/zerotrust-items-batch.json', 'utf-8'));
var client = new AWS.DynamoDB();
client.batchWriteItem(items, function(error, success) {
	if (error) {
		console.error('Error populating tables: ' + JSON.stringify(error) );
	}
	else {
		console.log('Populated tables: ' + JSON.stringify(success));
	}
});
