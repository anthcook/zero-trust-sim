'use strict';

const AWS = require( 'aws-sdk' );

var spinAC = new AWS.SharedIniFileCredentials( {profile: 'spin-ac'} );
AWS.config.update({
	region: 'us-east-1',
	//	credentials: spinAC
	endpoint: 'http://localhost:8000'
});

var args = process.argv.slice( 2 );
if (args != '') {
	var year = args[0];
	console.log( 'Querying movies for year ' + year );

	var docClient = new AWS.DynamoDB.DocumentClient();
	var params = {
		TableName: 'Movies',
		KeyConditionExpression: '#y = :yr',
		ExpressionAttributeNames: {
			'#y': 'year'
		},
		ExpressionAttributeValues: {
			':yr': Number(year)
		}
	};
	docClient.query( params, function(err, data) {
		if (err) {
			console.error( 'Query failed with error: ' + JSON.stringify(err) );
		}
		else {
			console.log( 'Result: ' + JSON.stringify(data) );
		}
	});
}
else {
	console.log( 'Must provide a year to query.' );
};
