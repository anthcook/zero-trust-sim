aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "1977"
	    	  },
		  "title": {
		    "S": "Episode IV - A New Hope"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "1980"
	    	  },
		  "title": {
		    "S": "Episode V - The Empire Strikes Back"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "1983"
	    	  },
		  "title": {
		    "S": "Episode VI - Return of the Jedi"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "1999"
	    	  },
		  "title": {
		    "S": "Episode I - The Phantom Menace"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "2002"
	    	  },
		  "title": {
		    "S": "Episode II - Attack of the Clones"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "2005"
	    	  },
		  "title": {
		    "S": "Episode III - Revenge of the Sith"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "2015"
	    	  },
		  "title": {
		    "S": "Episode VII - The Force Awakens"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "2017"
	    	  },
		  "title": {
		    "S": "Episode VIII - The Last Jedi"
	    	  }
  		}'

aws --profile spin-ac dynamodb put-item \
	--table-name Movies \
	--item \
		'{
		  "year": {
		    "N": "2019"
	    	  },
		  "title": {
		    "S": "Episode IX - The Rise of Skywalker"
	    	  }
  		}'

