aws dynamodb --profile spin-ac \
	create-table --table-name Movies \
		--attribute-definitions \
			AttributeName=year,AttributeType=N \
			AttributeName=title,AttributeType=S \
		--key-schema \
			AttributeName=year,KeyType=HASH \
			AttributeName=title,KeyType=RANGE \
		--billing-mode PROVISIONED \
		--provisioned-throughput \
			ReadCapacityUnits=10,WriteCapacityUnits=10

