'use strict';

angular.
	module('authorize', [
		'ec2monitorApp'
	]).
	component('authorize', {
		templateUrl: 'authorize/authorize.html',
		controller: ['appService', function AuthorizeController(appService) {
			var self = this;
			
			self.version = appService.version;
			self.clientId = '';
			self.accessKey = '';
			self.secretKey = '';
			self.sessionToken = '';
			self.region = '';

			var onSuccess = function(response) {
				appService.setReservations(response.data);
				appService.redirect('/monitor');
			};

			var onError = function(response) {
				alert( 'Error status received: ' + response.status );
			};

			self.authorize = function authorize() {
				if (self.accessKey !== '' && self.secretKey !== '') {
					appService.login(self.region, self.accessKey, self.secretKey, self.sessionToken, onSuccess, onError);
				}
				else {
					alert( 'An AWS access key and secret key are required.' );
				}
			};
		}]
	});
