'use strict';

angular.
	module('ec2monitorApp', [
		'ngRoute',
		'authorize',
		'monitor'
	]).
	config(['$routeProvider',
		function config($routeProvider) {
			$routeProvider.
				when('/authorize', {
					template: '<authorize></authorize>'
				}).
				when('/monitor', {
					template: '<monitor></monitor>'
				}).
				otherwise('/authorize');
		}
	]).
	service('appService', ['$http', '$location', function($http, $location) {
		this.version = '0.2.1';

		var clientId = '1';
		var reservations = {};

		this.setReservations = function(response) {
			reservations = response;
		}

		this.getReservations = function() {
			return reservations;
		}

		this.redirect = function(destination) {
			$location.path(destination).replace();
		}

		this.update = function(instance, onSuccess, onError) {
			var url = '/info?clientid=' + clientId +'&instanceid=' + instance;
			$http.get(url).then(onSuccess, onError);
		}

		this.login = function(region, accessKey, secretKey, sessionToken, onSuccess, onError) {
			var data =
				'clientid=' + clientId +
				'&region=' + region +
				'&accesskey=' + accessKey +
				'&secretkey=' + secretKey;
			if (sessionToken !== '') {
				data += '&sessiontoken=' + sessionToken;
			}
			postData('/login', data, onSuccess, onError);
		}

		this.runstate = function(instance, state, onSuccess, onError) {
			var data =
				'clientid=' + clientId +
				'&instanceid=' + instance +
				'&state=' + state;
			postData('/runstate', data, onSuccess, onError);
		}

		var postData = function(path, data, onSuccess, onError) {
			var config = {
				'headers': {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}};
			$http.post(path, data, config).then(onSuccess, onError);
		}
	}]);
