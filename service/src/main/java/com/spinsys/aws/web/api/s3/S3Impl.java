package com.spinsys.aws.web.api.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.internal.SSEResultBase;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.zerotrust.util.Parameters;


/**
 * Base implementation for AWS Simple Storage Service (S3) classes.
 */
public abstract class S3Impl
extends AWSImpl
{
	/**
	 * Returns the AWS S3 client instance.
	 * 
	 * @return the client instance.
	 */
	public synchronized AmazonS3 getS3Client()
	{
		if (s3 == null) {
			String region = getConfiguration().getAWSRegion();
			if (getConfiguration().hasS3Address()) {
				String endpoint = getConfiguration().getS3Address();
				s3 = AmazonS3ClientBuilder.standard().
					withCredentials( getCredentialsProvider() ).
					withEndpointConfiguration( new EndpointConfiguration(endpoint, region) ).
					withPathStyleAccessEnabled( true ).
					build();
			}
			else {
				s3 = AmazonS3ClientBuilder.standard().
					withCredentials( getCredentialsProvider() ).
					withRegion( Regions.fromName(region) ).
					build();
			}
		}

		return s3;
	}

	/**
	 * Formats and returns the given AWS API call result in the specified data
	 * format.
	 * 
	 * @param  result  the result to format.
	 * @param  format  the data format to return.
	 * @return  the result of the API call in the specified format.
	 */
	public String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format )
	{
		// Not supported
		return null;
	}

	/**
	 * Formats and returns the given Simple Storage Service (S3) API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	public abstract String format( SSEResultBase result, DataFormat format );


    protected S3Impl( Parameters config )
	throws AmazonClientException
    {
		super( config );
	}


	private AmazonS3 s3;
}
