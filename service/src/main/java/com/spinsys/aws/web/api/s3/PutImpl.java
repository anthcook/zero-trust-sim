package com.spinsys.aws.web.api.s3;

import java.io.File;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.internal.SSEResultBase;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Simple Storage Service (S3) API implementation to upload files to a storage
 * bucket.
 */
public class PutImpl
extends S3Impl
{
	/**
	 * Creates an instance of UploadImpl.
	 */
	public PutImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Uploads a file to a storage location on S3.
	 * 
	 * @return  AWS product pricing information.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AmazonS3 client = getS3Client();

		logInfo( LOG, "Getting started with Amazon S3 (upload)..." );

		String bucketName = getConfiguration().getS3BucketName();
		String objectName = getConfiguration().getS3ObjectName();
		File uploadFile = getConfiguration().getUploadFile();
		PutObjectRequest request = new PutObjectRequest( bucketName, objectName, uploadFile );
		PutObjectResult result = client.putObject( request );
		String response = format( result, getConfiguration().getOutputFormat() );

		logInfo( LOG, String.format("Received upload response:\n%s", response) );

		return response;
	}

	/**
	 * Formats and returns the given Simple Storage Service (S3) API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( SSEResultBase result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof PutObjectResult) {
			PutObjectResult por = (PutObjectResult) result;
			writer.addStringField( "ContentMD5", por.getContentMd5() );
			writer.addStringField( "ETag", por.getETag() );
			if (por.getExpirationTime() != null) {
				writer.addStringField( "ExpirationTime", DATETIME_FORMAT.format(por.getExpirationTime()) );
				writer.addStringField( "ExpirationTimeRuleID", por.getExpirationTimeRuleId() );
			}
			writer.addBooleanField( "RequesterCharged", por.isRequesterCharged() );
			writer.addStringField( "VersionID", por.getVersionId() );
			formatObjectMetadata( por.getMetadata(), writer );
		}
		else {
			writer.addStringField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected void formatObjectMetadata( ObjectMetadata metadata, JSONWriter writer )
	{
		if (metadata != null) {
			writer.startObject( "Metadata" );
			writer.addStringField( "ArchiveStatus", metadata.getArchiveStatus() );
			if (metadata.getBucketKeyEnabled() != null) {
				writer.addBooleanField( "BucketKeyEnabled", metadata.getBucketKeyEnabled() );
			}
			writer.addStringField( "CacheControl", metadata.getCacheControl() );
			writer.addStringField( "ContentDisposition", metadata.getContentDisposition() );
			writer.addStringField( "ContentEncoding", metadata.getContentEncoding() );
			writer.addNumberField( "ContentLength", metadata.getContentLength() );
			writer.addStringField( "ContentType", metadata.getContentType() );
			if (metadata.getHttpExpiresDate() != null) {
				writer.addStringField( "HttpExpiresDate", DATE_FORMAT.format(metadata.getHttpExpiresDate()) );
			}
			writer.addNumberField( "InstanceLength", metadata.getInstanceLength() );
			if (metadata.getLastModified() != null) {
				writer.addStringField( "LastModified", DATE_FORMAT.format(metadata.getLastModified()) );
			}
			writer.addStringField( "ObjectLockLegalHoldStatus", metadata.getObjectLockLegalHoldStatus() );
			writer.addStringField( "ObjectLockMode", metadata.getObjectLockMode() );
			if (metadata.getObjectLockRetainUntilDate() != null) {
				writer.addStringField( "ObjectLockRetailUntilDate", DATE_FORMAT.format(metadata.getObjectLockRetainUntilDate()) );
			}
			if (metadata.getOngoingRestore() != null) {
				writer.addBooleanField( "OngoingRestore", metadata.getOngoingRestore() );
			}
			if (metadata.getPartCount() != null) {
				writer.addNumberField( "PartCount", metadata.getPartCount() );
			}
			writer.addStringField( "ReplicationStatus", metadata.getReplicationStatus() );
			if (metadata.getRestoreExpirationTime() != null) {
				writer.addStringField( "RestoreExpirationTime", DATETIME_FORMAT.format(metadata.getRestoreExpirationTime()) );
			}
			writer.addStringField( "SSEAlgorithm", metadata.getSSEAlgorithm() );
			writer.addStringField( "SSEAwsKmsEncryptionContext", metadata.getSSEAwsKmsEncryptionContext() );
			writer.addStringField( "SSEAwsKmsKeyID", metadata.getSSEAwsKmsKeyId() );
			writer.addStringField( "SSECustomerAlgorithm", metadata.getSSECustomerAlgorithm() );
			writer.addStringField( "SSECustomerKeyMD5", metadata.getSSECustomerKeyMd5() );
			writer.addStringField( "StorageClass", metadata.getStorageClass() );
			formatContentRange( metadata.getContentRange(), writer );
			formatRawMetadata( metadata.getRawMetadata(), writer );
			formatUserMetadata( metadata.getUserMetadata(), writer );
			writer.endObject();
		}
	}

	protected void formatContentRange( Long[] values, JSONWriter writer )
	{
		if (values != null) {
			writer.startList( "ContentRange" );
			for (int i = 0; i < values.length; i++) {
				writer.addElement( "\"" + values[i] + "\"" );
			}
			writer.endList();
		}
	}

	protected void formatRawMetadata( Map<String,Object> rmetadata, JSONWriter writer )
	{
		if (rmetadata != null) {
			writer.startObject( "RawMetadata" );
			for (String key: rmetadata.keySet()) {
				writer.addStringField( key, String.valueOf(rmetadata.get(key)) );
			}
			writer.endObject();
		}
	}

	protected void formatUserMetadata( Map<String,String> umetadata, JSONWriter writer )
	{
		if (umetadata != null) {
			writer.startObject( "UserMetadata" );
			for (String key: umetadata.keySet()) {
				writer.addStringField( key, String.valueOf(umetadata.get(key)) );
			}
			writer.endObject();
		}
	}


	private static final Log LOG = LogFactory.getLog( PutImpl.class );
}
