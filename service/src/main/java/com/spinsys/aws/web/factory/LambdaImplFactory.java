package com.spinsys.aws.web.factory;

import com.spinsys.aws.web.api.lambda.InvokeBrokerImpl;
import com.spinsys.zerotrust.util.Parameters;

public class LambdaImplFactory
{
	/**
	 * Return an instance of the AWS Lambda API invocation implementation class
	 * for calling the MDACA Vitro IoT lambda broker.
	 * 
	 * @param requesterId  the function caller's identifier.
	 * @param config  the configuration parameters.
	 * @return  the corresponding API implementation class.
	 */
	public static InvokeBrokerImpl getBrokerImplementation( String requesterId, Parameters config )
	{
		return new InvokeBrokerImpl( config, requesterId );
	}
}
