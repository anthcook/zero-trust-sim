package com.spinsys.aws.web.api.s3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.internal.SSEResultBase;
import com.amazonaws.services.s3.model.ExpressionType;
import com.amazonaws.services.s3.model.InputSerialization;
import com.amazonaws.services.s3.model.OutputSerialization;
import com.amazonaws.services.s3.model.SelectObjectContentEvent;
import com.amazonaws.services.s3.model.SelectObjectContentEventVisitor;
import com.amazonaws.services.s3.model.SelectObjectContentRequest;
import com.amazonaws.services.s3.model.SelectObjectContentResult;
import com.amazonaws.util.IOUtils;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Simple Storage Service (S3) API base implementation to retrieve selected
 * content from files in a storage bucket.
 */
public abstract class BaseSelectImpl
extends S3Impl
{
	/**
	 * Creates an instance of SelectImpl.
	 */
	public BaseSelectImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Returns the contents of selected files from a storage location on S3.
	 * 
	 * @return  file contents.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AmazonS3 client = getS3Client();

		logInfo( LOG, "Getting started with Amazon S3 (select)..." );

		final AtomicBoolean isResultComplete = new AtomicBoolean( false );
		SelectObjectContentResult result = client.selectObjectContent( generateBaseRequest() );
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream resultInputStream = result.getPayload().
			getRecordsInputStream( new SelectObjectContentEventVisitor() {
				@Override
				public void visit( SelectObjectContentEvent.StatsEvent event )
				{
					logInfo( LOG, String.format(
						"Received Stats: Bytes Scanned = %s; Bytes Processed = %s",
						event.getDetails().getBytesScanned(),
						event.getDetails().getBytesProcessed()) );
					}
	
					/*
					 * An End Event informs that the request has finished successfully.
					 */
					@Override
					public void visit( SelectObjectContentEvent.EndEvent event )
					{
						isResultComplete.set( true );
						logInfo( LOG, "Received End Event. Result is complete." );
					}
				}
			);
	
		try {
			IOUtils.copy( resultInputStream, baos );
		}
		catch (IOException e) {
			throw new AmazonServiceException( "Error copying result stream for output.", e );
		}
	
		/*
		 * The End Event indicates all matching records have been transmitted.
		 * If the End Event is not received, the results may be incomplete.
		 */
		if (!isResultComplete.get()) {
			throw new AmazonServiceException( "End event not received for S3 select request: bucket = '%s', key = '%s', query = '%s'." );
		}

		String response = format( baos.toByteArray(), getConfiguration().getOutputFormat() );

		logInfo( LOG, String.format("Received select response:\n%s", response) );

		return response;
	}

	/**
	 * Formats and returns the given Simple Storage Service (S3) select result
	 * data in the specified format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	public String format( byte[] resultData, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.startObject( "SelectResult" );
		writer.addElement( "\"" + new String(resultData) + "\"" );
		writer.endObject();
		writer.endDocument();
		
		return writer.toString();
	}

	/**
	 * Not supported by this implementation.
	 * 
	 * @return  null
	 */
	public String format( SSEResultBase result, DataFormat format )
	{
		return null;
	}

	
	protected SelectObjectContentRequest generateBaseRequest()
	{
		SelectObjectContentRequest request = new SelectObjectContentRequest();
		request.setBucketName( getConfiguration().getS3BucketName() );
		request.setKey( getConfiguration().getS3ObjectName() );
		request.setExpression( getConfiguration().getS3SelectStatement() );
		request.setExpressionType( ExpressionType.SQL );

		request.setInputSerialization( generateInputSerialization() );
		request.setOutputSerialization( generateOutputSerialization() );

		return request;
	}

	protected abstract InputSerialization generateInputSerialization();

	protected abstract OutputSerialization generateOutputSerialization();


	private static final Log LOG = LogFactory.getLog( BaseSelectImpl.class );
}
