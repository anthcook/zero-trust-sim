package com.spinsys.aws.web.api.lambda;

import java.io.IOException;
import java.util.Base64;
import java.util.Iterator;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.util.Utils;
import com.spinsys.zerotrust.broker.db.Customer;
import com.spinsys.zerotrust.broker.security.CryptoKeys;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * AWS Lambda API implementation to invoke the MDACA Vitro IoT Lambda broker
 * implementation.
 */
public class InvokeBrokerImpl
extends LambdaImpl
{
	/**
	 * Creates an instance of InvokeBrokerImpl.
	 */
	public InvokeBrokerImpl( Parameters configuration, String requesterId )
	{
		super( configuration );

		this.requesterId = requesterId;
	}

	
	/**
	 * Invokes the configured MDACA Vitro IoT lambda broker function with the
	 * configured payload.
	 * 
	 * @return  the broker's invocation result.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		logInfo( LOG, "Getting started with AWS Lambda (invoke Vitro IoT Broker)..." );

		String response;
		String functionName = getConfiguration().getBrokerAddress();
		switch (getConfiguration().getBrokerRequestType(requesterId)) {
			case DECRYPT_IOT:
			case VALIDATE_IOT:
				response = invoke( functionName, makeNodeDataPayload() );
				break;
			case SECURE_KEYS:
				response = invoke( functionName, makeCryptoKeysPayload() );
				break;
			case UPDATE_CUSTOMER:
				response = invoke( functionName, makeCustomerInfoPayload() );
				break;
			default:
				throw new AmazonClientException( String.format("Unsupported broker request type '%s'.", getConfiguration().getBrokerRequestType(requesterId).toString()) );
		}

		logInfo( LOG, String.format("Received response:\n%s", response) );

		return response;
	}


	/**
	 * Formats and returns the given 
	 * @return
	 */
	protected String makeNodeDataPayload()
	{
		JSONWriter writer = startEventWriter();
		writer.startObject( "data" );
		writer.startList( "nodeBlocks" );
		Iterator<String> it = getConfiguration().getBrokerRequestPayload( requesterId ).getNodeData().iterator();
		while (it.hasNext()) {
			writer.addElement( "\"" + it.next() + "\"" );
		}
		writer.endList();
		writer.endObject();
		writer.endDocument();

		return writer.toString();
	}

	protected String makeCryptoKeysPayload()
	throws AmazonServiceException
	{
		JSONWriter writer = startEventWriter();
		try {
			Base64.Encoder encoder = Base64.getEncoder();
			CryptoKeys keys = getConfiguration().getBrokerRequestPayload( requesterId ).getCryptoKeys();
			writer.startObject( "keys" );
			writer.addStringField( "nodeId", requesterId );
			writer.addStringField( "encryptionKey", encoder.encodeToString(Utils.toByteArray(keys.symmetricKey)) );
			writer.addStringField( "signatureKey", encoder.encodeToString(Utils.toByteArray(keys.publicKey)) );
			writer.endObject();
		}
		catch (IOException exc) {
			throw new AmazonServiceException( "Error making crypto keys payload.", exc );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected String makeCustomerInfoPayload()
	{
		JSONWriter writer = startEventWriter();
		Customer info = getConfiguration().getBrokerRequestPayload( requesterId ).getCustomerInfo();
		writer.startObject( "customer" );
		writer.addStringField( "nodeId", info.getDeviceID() );
		writer.addStringField( "customerId", info.getCustomerID() );
		writer.addStringField( "customerAccountId", info.getAccountID() );
		writer.endObject();
		writer.endDocument();

		return writer.toString();
	}

	protected JSONWriter startEventWriter()
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.addStringField( "event", getConfiguration().getBrokerRequestType(requesterId).getRequestPath() );

		return writer;
	}

	private final String requesterId;

	private static final Log LOG = LogFactory.getLog( InvokeBrokerImpl.class );
}
