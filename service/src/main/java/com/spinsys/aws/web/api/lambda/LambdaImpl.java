package com.spinsys.aws.web.api.lambda;

import java.nio.ByteBuffer;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.ServiceException;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.text.JSONWriter;


/**
 * Base implementation for AWS Lambda classes.
 */
public abstract class LambdaImpl
extends AWSImpl
{
	/**
	 * Returns the AWS Lambda client instance.
	 * 
	 * @return the client instance.
	 */
	public synchronized AWSLambda getLambdaClient()
	{
		if (lambda == null) {
			lambda = AWSLambdaClientBuilder.standard().
				withCredentials( getCredentialsProvider() ).
				withRegion( Regions.fromName(getConfiguration().getAWSRegion()) ).
				build();
		}

		return lambda;
	}

	/**
	 * Invokes the named broker function with the given payload.
	 * 
	 * @param name  the function name.
	 * @param payload  the function payload.
	 * @return  the function's response.
	 * @throws ServiceException
	 */
	public String invoke( String name, String payload )
	throws ServiceException
	{
		InvokeRequest request = new InvokeRequest().
			withFunctionName( name ).withPayload( payload );

		InvokeResult result = getLambdaClient().invoke( request );

		return format( result );
	}

	/**
	 * Formats and returns the given AWS lambda function call result in the
	 * specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( AmazonWebServiceResult<ResponseMetadata> result, Constants.DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof InvokeResult) {
			formatInvokeResult( (InvokeResult)result, writer );
		}
		else {
			writer.addQuotedField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}


	protected void formatInvokeResult( InvokeResult result, JSONWriter writer )
	{
		writer.addStringField( "ExecutedVersion", result.getExecutedVersion() );
		if (result.getFunctionError() != null) {
			writer.addStringField( "FunctionError", result.getFunctionError() );
		}
		ByteBuffer payload = result.getPayload();
		if (payload != null) {
			String payloadStr;
			if (payload.hasArray()) {
				payloadStr = new String( payload.array() );
			}
			else {
				payloadStr = payload.toString();
			}
			writer.addElement( "\"Payload\": " + payloadStr );
		}
		writer.addNumberField( "StatusCode", result.getStatusCode() );
		if (result.getLogResult() != null) {
			writer.startObject( "LogResult" );
			writer.addElement( "\"" + result.getLogResult() + "\"" );
			writer.endObject();
		}
	}


    protected LambdaImpl( Parameters config )
	throws AmazonClientException
    {
		super( config );
	}


	private AWSLambda lambda;
}
