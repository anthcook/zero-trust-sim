package com.spinsys.aws.web.api.s3;

import com.amazonaws.services.s3.model.CompressionType;
import com.amazonaws.services.s3.model.InputSerialization;
import com.amazonaws.services.s3.model.JSONInput;
import com.amazonaws.services.s3.model.JSONOutput;
import com.amazonaws.services.s3.model.JSONType;
import com.amazonaws.services.s3.model.OutputSerialization;
import com.spinsys.zerotrust.util.Parameters;


/**
 * Simple Storage Service (S3) API implementation to retrieve selected data
 * from JSON files in a storage bucket.
 */
public class JSONSelectImpl
extends BaseSelectImpl
{
	/**
	 * Creates an instance to select data from JSON files in S3.
	 */
	public JSONSelectImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	protected InputSerialization generateInputSerialization()
	{
		InputSerialization inputSerialization = new InputSerialization();
		inputSerialization.setJson( new JSONInput().withType(JSONType.DOCUMENT) );
		inputSerialization.setCompressionType( CompressionType.NONE );

		return inputSerialization;
	}

	protected OutputSerialization generateOutputSerialization()
	{
		OutputSerialization outputSerialization = new OutputSerialization();
		outputSerialization.setJson( new JSONOutput().withRecordDelimiter(",") );

		return outputSerialization;
	}
}
