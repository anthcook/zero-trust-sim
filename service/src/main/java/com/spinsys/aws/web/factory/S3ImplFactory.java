package com.spinsys.aws.web.factory;

import com.spinsys.aws.web.api.s3.JSONSelectImpl;
import com.spinsys.aws.web.api.s3.PutImpl;
import com.spinsys.aws.web.api.s3.S3Impl;
import com.spinsys.zerotrust.util.Parameters;
import com.spinsys.zerotrust.util.Constants.DataFormat;
import com.spinsys.zerotrust.util.Constants.S3Action;

public class S3ImplFactory
{
	/**
	 * Return an instance of the Amazon Simple Storage Service (S3) API
	 * implementation class specified by the given action type.
	 * 
	 * @param actionType  the S3 action to perform.
	 * @return  the corresponding API implementation class.
	 */
	public static S3Impl getS3Implementation( S3Action actionType, Parameters config )
	{
		S3Impl impl;
		switch (actionType) {
			case DELETE:
				// TODO
			case GET:
				// TODO
			case LIST:
				//impl = new ListImpl( config );
				throw new RuntimeException( "TODO: unsupported action type " + actionType );
			case PUT:
				impl = new PutImpl( config );
				break;
			case SELECT:
				DataFormat format = config.getOutputFormat();
				if (format == DataFormat.JSON) {
					impl = new JSONSelectImpl( config );
				}
				else {
					throw new RuntimeException( "TODO: unsupported select output format " + format.name() );
				}
				break;
			default:
				impl = null;
		}

		return impl;
	}
}
