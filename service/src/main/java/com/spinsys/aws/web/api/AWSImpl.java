package com.spinsys.aws.web.api;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;

import org.apache.commons.logging.Log;


/**
 * Base implementation for AWS API classes.
 */
public abstract class AWSImpl
implements Constants
{
	/**
	 * Returns the API call configuration parameters.
	 * 
	 * @return  the configuration parameters.
	 */
	public Parameters getConfiguration()
	{
		return config;
	}
	
	/**
	 * Returns the credentials provider.
	 * 
	 * @return  the credentials provider.
	 */
	public AWSCredentialsProvider getCredentialsProvider()
	{
		return credentialsProvider;
	}

	/**
	 * Logs the given information message to the provided implementation logger.
	 * 
	 * @param  logger  the implementation logger.
	 * @param  mesg  the information message.
	 */
	public void logInfo( Log logger, String message )
	{
		if (logger.isInfoEnabled()) {
			logger.info( message );
		}
	}

	/**
	 * Formats and returns the given AWS API call result in the default data
	 * format, JSON.
	 * 
	 * @param  result  the result to format.
	 * @return  the result of the API call in JSON format.
	 */
	public String format( AmazonWebServiceResult<ResponseMetadata> result )
	{
		return format( result, DataFormat.JSON );
	}

	/**
	 * Performs an AWS API call and returns the result.
	 * 
	 * Throws an exception if the API request was rejected with an error
	 * response, or if a serious internal problem occurred while trying to
	 * communicate with the service.
	 * 
	 * @return  the result of the API call or null if no result is received.
	 * @throws  com.amazonaws.AmazonServiceException
	 *            if the request is rejected with an error.
	 * @throws  com.amazonaws.AmazonClientException
	 *            if a serious internal error occurs.
	 */
	public abstract String execute() throws AmazonServiceException, AmazonClientException;

	/**
	 * Formats and returns the given AWS API call result in the specified data
	 * format.
	 * 
	 * @param  result  the result to format.
	 * @param  format  the data format to return.
	 * @return  the result of the API call in the specified format.
	 */
	public abstract String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format );


    protected AWSImpl( Parameters config )
	throws AmazonClientException
    {
		if (config.hasAWSConfigCredentials()) {
			if (config.hasAWSSessionToken()) {
				BasicSessionCredentials credentials = new BasicSessionCredentials(
					config.getAWSAccessKey(), config.getAWSSecretKey(), config.getAWSSessionToken() );
				credentialsProvider = new AWSStaticCredentialsProvider( credentials );
			}
			else {
				System.setProperty( AWS_ACCESS_KEY_ID_PROPERTY, config.getAWSAccessKey() );
				System.setProperty( AWS_SECRET_ACCESS_KEY_PROPERTY, config.getAWSSecretKey() );
				credentialsProvider = new SystemPropertiesCredentialsProvider();
			}
		}
		else {
			String profile = config.getAWSSecurityProfile();
			if (profile != null) {
				credentialsProvider = new ProfileCredentialsProvider( profile );
			}
			else {
				credentialsProvider = new ProfileCredentialsProvider();
			}
		}

		try {
			credentialsProvider.getCredentials();
		}
		catch (Exception e) {
			StringBuilder mesg = new StringBuilder().
				append( "Credentials not found. Verify credentials file is at the " ).
				append( "correct location (~/.aws/credentials), and is in valid format, " ).
				append( "provide credentials in configuration" );
			throw new AmazonClientException( mesg.toString(), e );
		}

		this.config = config;
	}


	private Parameters config;
	private AWSCredentialsProvider credentialsProvider;
}
