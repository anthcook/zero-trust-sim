package com.spinsys.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.util.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spinsys.zerotrust.broker.security.CryptoKeys;
import com.spinsys.zerotrust.io.NodeBlock;
import com.veetechis.lib.util.ArrayConverter;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class Utils
{
	/**
	 * Serializes the given object into an array of bytes.
	 * 
	 * @param obj  the object to serialize.
	 * @return  the serialized array of bytes.
	 */
	public static byte[] toByteArray( Object obj )
	throws IOException
	{
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream oout = new ObjectOutputStream( bout );
		oout.writeObject( obj );
		oout.flush();
		oout.close();
		bout.close();

		return bout.toByteArray();
	}

	/**
	 * Returns the given JSON document in pretty print format. Returns the
	 * document in original form if an error occurs and logs the error.
	 * 
	 * @param  json
	 * @return  pretty print version of the document, or original in event of
	 *            an error.
	 */
	public static String beautify( String json )
	{
		String rjson = json;

		ObjectMapper mapper = new ObjectMapper();

		try {
			Object obj = mapper.readValue( json, Object.class );
			rjson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString( obj );
		}
		catch (JsonProcessingException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error processing JSON document: %s", json), e );
			}
		}

		return rjson;
	}

	/**
	 * Copies <code>length</code> number of bytes from the the given source
	 * buffer to the provided field buffer, starting from the beginning of
	 * the source buffer.
	 * 
	 * Throws an exception if <code>length</code> exceeds the size of the
	 * field buffer, or if <code>length</code> is greater than the size of the
	 * source buffer.
	 */
	public static void copyData( byte[] field, byte[] source, int length )
	{
		copyData( field, source, 0, length );
	}

	/**
	 * Copies <code>length</code> number of bytes from the the given source
	 * buffer to the provided field buffer, starting from position
	 * <code>offset</code> in the source buffer.
	 * 
	 * Throws an exception if <code>length</code> exceeds the size of the
	 * field buffer, or if the sum of <code>offset + length</code> is greater
	 * than the size of the source buffer.
	 */
	public static void copyData( byte[] field, byte[] source, int offset, int length )
	{
		System.arraycopy( source, offset, field, 0, length );
	}

	/**
	 * Copies <code>length</code> number of bytes from the the given source
	 * buffer to position <code>offset</code> the provided field buffer,
	 * starting from position 0 in the source buffer.
	 * 
	 * Throws an exception if <code>length</code> exceeds the size of the
	 * field buffer, or if the sum of <code>offset + length</code> is greater
	 * than the size of the source buffer.
	 */
	public static void copyData( byte[] field, int offset, byte[] source, int length )
	{
		System.arraycopy( source, 0, field, offset, length );
	}

	public static String toCamelCase( String word )
	{
		return new StringBuilder().
			append( word.substring(0,1) ).
			append( word.substring(1).toLowerCase() ).
			toString();
	}

	public static void dumpData( NodeBlock nodeBlock )
	{
		long timestamp = (long) ArrayConverter.bytesToInt( nodeBlock.getTimestamp() );
		LOG.info( String.format("Dumping node block (size = %d):", nodeBlock.size()) );
		LOG.info( String.format("PID: %s", Hex.encodeHexString(nodeBlock.getPID())) );
		LOG.info( String.format("ID: %s", Hex.encodeHexString(nodeBlock.getID())) );
		LOG.info( String.format("Timestamp (raw): %s", timestamp) );
		LOG.info( String.format("Timestamp: %s", Instant.ofEpochSecond(timestamp).toString()) );
		LOG.info( String.format("IV: %s", Hex.encodeHexString(nodeBlock.getIV())) );
		LOG.info( String.format("Data: %s", Hex.encodeHexString(nodeBlock.getData())) );
		LOG.info( String.format("Digest: %s", Hex.encodeHexString(nodeBlock.getDigest())) );
		LOG.info( String.format("Signature: %s", Hex.encodeHexString(nodeBlock.getSignature())) );
		LOG.info( String.format("Encoded: %s", nodeBlock.toString()) );
	}

	public static void dumpData( CryptoKeys keys )
	{
		try {
			byte[] pubKey = toByteArray( keys.publicKey );
			byte[] priKey = toByteArray( keys.privateKey );
			byte[] symKey = toByteArray( keys.symmetricKey );
			byte[] intVec = keys.initVector.getIV();
			LOG.info( "Dumping crypto keys:" );
			LOG.info( String.format("Public key: %s", Hex.encodeHexString(pubKey)) );
			LOG.info( String.format("Private key: %s", Hex.encodeHexString(priKey)) );
			LOG.info( String.format("Symmetric key: %s", Hex.encodeHexString(symKey)) );
			LOG.info( String.format("Init Vector: %s", Hex.encodeHexString(intVec)) );
			LOG.info( String.format("Encoded: %s%s%s%s", new Object[] {
				Base64.getEncoder().encodeToString(pubKey),
				Base64.getEncoder().encodeToString(priKey),
				Base64.getEncoder().encodeToString(symKey),
				Base64.getEncoder().encodeToString(intVec) }) );
		}
		catch (Exception e) {
			LOG.error( "Error dumping crypto keys.", e );
		}
	}


	private static final Log LOG = LogFactory.getLog( Utils.class );
}
