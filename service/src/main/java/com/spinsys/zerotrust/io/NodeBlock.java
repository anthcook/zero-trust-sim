package com.spinsys.zerotrust.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import com.spinsys.util.Utils;
import com.spinsys.zerotrust.util.Constants.DataFormat;
import com.veetechis.lib.text.JSONWriter;
import com.veetechis.lib.util.ArrayConverter;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Represents a non-publicly mutable Vitro Crystal Gateway IoT sensor block
 * message. The message is a binary stream with the following structure:
 * 
 * ECCID(9)|Timestamp(4)|IV(16)|Data(?)|Digest(32)|Signature(64)
 * <ol>
 * <li>ECCID ("NodeID") = 9 byte device ID</li>
 * <li>Timestamp = 4 byte block timestamp</li>
 * <li>IV = 16 byte symmetric key initialization vector</li>
 * <li>Data = variable length base64 encoded AES256/CBC encrypted sensor data</li>
 * <li>Digest = 32 byte SHA256 block hash (1|2|3|4)</li>
 * <li>Signature = 64 byte ASN.1 DER/X509 digest signature</li>
 * </ol>
 */
public class NodeBlock
{
	// Raw data
	public static final int PARAM_ID_SIZE = 2;
	public static final int SENSOR_DATA_SIZE = 4;

	// Block structure data (141)
	public static final int ECC_ID_SIZE = 9;
	public static final int TIMESTAMP_SIZE = 4;
	public static final int IV_SIZE = 16;
	public static final int ENCRYPTED_DATA_SIZE = 16;
	public static final int HASH_SIZE = 32;
	public static final int SIGNATURE_SIZE = 64;

	// Digestable block size (47)
	public static final int DIGESTABLE_LENGTH =
		IV_SIZE + ENCRYPTED_DATA_SIZE + PARAM_ID_SIZE + ECC_ID_SIZE + TIMESTAMP_SIZE;

	// Final block size (PID + Block: 143)
	public static final int MINIMUM_LENGTH =
		DIGESTABLE_LENGTH + HASH_SIZE + SIGNATURE_SIZE;
		
	public static final String EVENT_CLIENT_ID_FLD = "clientId";
	public static final String EVENT_NODE_ID_FLD = "deviceId";
	public static final String EVENT_TIMESTAMP_FLD = "timestamp";
	public static final String EVENT_DATA_VALUE_FLD = "data";

    public static final Schema AVRO_SCHEMA = SchemaBuilder.record("AvroNodeRecord")
            .namespace("com.spinsys.zerotrust.vitro.model.avro").fields()
            .requiredString("clientId").requiredString("deviceId")
            .requiredString("data").requiredString("timestamp")
            .endRecord();

	/**
	 * Converts the given raw IoT message data into a new instance.
	 * 
	 * Throws an exception if <code>data</code> is null or if it's length is
	 * less than the minimum expected block size.
	 * 
	 * @param data  the raw data to convert.
	 * @return  a new instance.
	 */
	public static NodeBlock getInstance( byte[] data )
	throws IOException, NullPointerException
	{
		NodeBlock block = null;

		if (data.length < MINIMUM_LENGTH) {
			throw new IOException(
				String.format("Data length (%d) invalid for conversion to node block.",
					data.length) );
		}

		block = new NodeBlock();
		int offset = 0;
		// 0. Parameter ID
		Utils.copyData( block.getPIDBuffer(), data, offset, PARAM_ID_SIZE );
		offset += PARAM_ID_SIZE;
		// 1. ECC ID [NodeID/DeviceID]
		Utils.copyData( block.getIDBuffer(), data, offset, ECC_ID_SIZE );
		offset += ECC_ID_SIZE;
		// 2. Timestamp
		Utils.copyData( block.getTimestampBuffer(), data, offset, TIMESTAMP_SIZE );
		offset += TIMESTAMP_SIZE;
		// 3. Initialization Vector (IV)
		Utils.copyData( block.getIVBuffer(), data, offset, IV_SIZE );
		offset += IV_SIZE;
		// 4. Data
		int dataLen = data.length - (MINIMUM_LENGTH - ENCRYPTED_DATA_SIZE);
		Utils.copyData( block.getDataBuffer(dataLen), data, offset, dataLen );
		offset += dataLen;
		// 5. Digest/Hash
		Utils.copyData( block.getDigestBuffer(), data, offset, HASH_SIZE );
		offset += HASH_SIZE;
		// 6. Signature
		Utils.copyData( block.getSignatureBuffer(), data, offset, SIGNATURE_SIZE );

		return block;
	}


	protected NodeBlock()
	{
		super();

		pid = new byte[NodeBlock.PARAM_ID_SIZE];
		digest = new byte[NodeBlock.HASH_SIZE];
		nodeIv = new byte[NodeBlock.IV_SIZE];
		nodeId = new byte[NodeBlock.ECC_ID_SIZE];
		signature = new byte[NodeBlock.SIGNATURE_SIZE];
		timestamp = new byte[NodeBlock.TIMESTAMP_SIZE];
	}


	public byte[] getData()
	{
		if (nodeDataValue == null && nodeData != null) {
			nodeDataValue = new byte[nodeData.length];
			Utils.copyData( nodeDataValue, nodeData, nodeData.length );
		}

		return nodeDataValue;
	}
	
	public byte[] getPID()
	{
		if (paramIdValue == null) {
			paramIdValue = new byte[PARAM_ID_SIZE];
			Utils.copyData( paramIdValue, pid, PARAM_ID_SIZE );
		}

		return paramIdValue;
	}
	
	public String getHexPID()
	{
		return Hex.encodeHexString( getPID() );
	}
	
	public byte[] getID()
	{
		if (nodeIdValue == null) {
			nodeIdValue = new byte[ECC_ID_SIZE];
			Utils.copyData( nodeIdValue, nodeId, ECC_ID_SIZE );
		}

		return nodeIdValue;
	}
	
	public String getHexID()
	{
		return Hex.encodeHexString( getID() );
	}
	
	public byte[] getTimestamp()
	{
		if (timestampValue == null) {
			timestampValue = new byte[TIMESTAMP_SIZE];
			Utils.copyData( timestampValue, timestamp, TIMESTAMP_SIZE );
		}

		return timestampValue;
	}
	
	public int getIntegerTimestamp()
	{
		return ArrayConverter.bytesToInt( getTimestamp() );
	}

	public byte[] getIV()
	{
		if (nodeIvValue == null) {
			nodeIvValue = new byte[IV_SIZE];
			Utils.copyData( nodeIvValue, nodeIv, IV_SIZE );
		}

		return nodeIvValue;
	}
	
	public byte[] getDigest()
	{
		if (digestValue == null) {
			digestValue = new byte[HASH_SIZE];
			Utils.copyData( digestValue, digest, HASH_SIZE );
		}

		return digestValue;
	}
	
	public byte[] getSignature()
	{
		if (signatureValue == null) {
			signatureValue = new byte[SIGNATURE_SIZE];
			Utils.copyData( signatureValue, signature, SIGNATURE_SIZE );
		}

		return signatureValue;
	}

	public int size()
	{
		return getPID().length + getID().length + getTimestamp().length +
			getIV().length + getData().length + getDigest().length +
			getSignature().length;
	}

	/**
	 * Returns a digestable data structure per Vitro's documentation...
	 * "Data which is hashed is Initialization Vector (16 bytes), encrypted
	 * data (16 bytes[+]), Parameter ID (2 bytes), Node ID (9 bytes) and
	 * timestamp (4 bytes). It is collected into structure which has exactly
	 * this order..."
	 */
	public byte[] toDigestRecord()
	throws IOException
	{
		byte[] data = getData();
		byte[] record = new byte[DIGESTABLE_LENGTH + (data.length - ENCRYPTED_DATA_SIZE)];
		int offset = 0;

		// 3. Initialization Vector (IV)
		Utils.copyData( record, offset, getIV(), IV_SIZE );
		offset += IV_SIZE;

		// 4. Data
		Utils.copyData( record, offset, data, data.length );
		offset += data.length;

		// 0. Param ID
		Utils.copyData( record, offset, getPID(), PARAM_ID_SIZE );
		offset += PARAM_ID_SIZE;

		// 1. ECC ID [NodeID/DeviceID]
		Utils.copyData( record, offset, getID(), ECC_ID_SIZE );
		offset += ECC_ID_SIZE;

		// 2. Timestamp
		Utils.copyData( record, offset, getTimestamp(), TIMESTAMP_SIZE );
		//offset += TIMESTAMP_SIZE;

		return record;
	}

	public byte[] toRawRecord()
	throws IOException
	{
		byte[] record = new byte[size()];
		int offset = 0;

		// 0. Param ID
		Utils.copyData( record, offset, getPID(), PARAM_ID_SIZE );
		offset += PARAM_ID_SIZE;

		// 1. ECC ID [NodeID/DeviceID]
		Utils.copyData( record, offset, getID(), ECC_ID_SIZE );
		offset += ECC_ID_SIZE;

		// 2. Timestamp
		Utils.copyData( record, offset, getTimestamp(), TIMESTAMP_SIZE );
		offset += TIMESTAMP_SIZE;

		// 3. Initialization Vector (IV)
		Utils.copyData( record, offset, getIV(), IV_SIZE );
		offset += IV_SIZE;

		// 4. Data
		byte[] data = getData();
		Utils.copyData( record, offset, data, data.length );
		offset += data.length;

		// 5. Digest/Hash
		Utils.copyData( record, offset, getDigest(), HASH_SIZE );
		offset += HASH_SIZE;
		
		// 6. Signature
		Utils.copyData( record, offset, getSignature(), SIGNATURE_SIZE );

		return record;
	}

	/**
	 * Returns the instance in Vitro JSON event object format.
	 */
	public byte[] toEventObject( String clientId, DataFormat format )
	{
		byte[] evtObj;

		switch (format) {
			case AVRO:
				evtObj = toAvroEvent( clientId );
				break;
			case JSON:
				evtObj = toJsonEvent( clientId );
				break;
			default:
				evtObj = "".getBytes();
				if (LOG.isWarnEnabled()) {
					LOG.warn( String.format("TODO: Unsupported format: %s", format) );
				}
		}

		return evtObj;
	}

	/**
	 * Returns the instance raw record in Base64 encoded form.
	 */
	public String toString()
	{
		String value;

		try {
			value = Base64.getEncoder().encodeToString( toRawRecord() );
		}
		catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( "Error while encoding to String.", e );
			}

			value = "";
		}

		return value;
	}


	protected byte[] toAvroEvent( String clientId )
	{
		byte[] event;

		GenericRecord rec = new GenericData.Record( AVRO_SCHEMA );
		rec.put( EVENT_CLIENT_ID_FLD, clientId );
		rec.put( EVENT_NODE_ID_FLD, getHexID() );
		rec.put( EVENT_TIMESTAMP_FLD, Integer.toString(getIntegerTimestamp()) );
		rec.put( EVENT_DATA_VALUE_FLD, toString() );

		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>( AVRO_SCHEMA );
		DataFileWriter<GenericRecord> writer = new DataFileWriter<GenericRecord>(datumWriter);
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			writer.create( AVRO_SCHEMA, out );
			writer.append( rec );
			writer.close();
			event = out.toByteArray();
		}
		catch (IOException e) {
			event = "".getBytes();
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error creating AVRO event.", e) );
			}
		}

		return event;
	}

	protected byte[] toJsonEvent( String clientId )
	{
		JSONWriter writer = new JSONWriter();
		writer.startObject();
		writer.addStringField( EVENT_CLIENT_ID_FLD, clientId );
		writer.addStringField( EVENT_NODE_ID_FLD, getHexID() );
		writer.addStringField( EVENT_TIMESTAMP_FLD, Integer.toString(getIntegerTimestamp()) );
		writer.addStringField( EVENT_DATA_VALUE_FLD, toString() );
		writer.endObject();

		return writer.toString().getBytes();
	}

	protected byte[] getDataBuffer( int size )
	{
		if (nodeData == null) {
			nodeData = new byte[size];
		}

		return nodeData;
	}

	protected byte[] getPIDBuffer()
	{
		return pid;
	}

	protected byte[] getIDBuffer()
	{
		return nodeId;
	}

	protected byte[] getTimestampBuffer()
	{
		return timestamp;
	}

	protected byte[] getIVBuffer()
	{
		return nodeIv;
	}

	protected byte[] getDigestBuffer()
	{
		return digest;
	}

	protected byte[] getSignatureBuffer()
	{
		return signature;
	}


	private byte[] paramIdValue;
	private byte[] nodeDataValue;
	private byte[] digestValue;
	private byte[] nodeIvValue;
	private byte[] nodeIdValue;
	private byte[] signatureValue;
	private byte[] timestampValue;

	private byte[] nodeData;
	private final byte[] pid;
	private final byte[] digest;
	private final byte[] nodeIv;
	private final byte[] nodeId;
	private final byte[] signature;
	private final byte[] timestamp;


	private static final Log LOG = LogFactory.getLog( NodeBlock.class );
}
