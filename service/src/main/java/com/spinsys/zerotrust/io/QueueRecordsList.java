package com.spinsys.zerotrust.io;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.spinsys.zerotrust.util.Constants.DataFormat;
import com.veetechis.lib.security.Encoder;
import com.veetechis.lib.text.JSONWriter;

public class QueueRecordsList
extends ArrayList<NodeBlock>
{
	protected static final String HEADER = "Records";

	public QueueRecordsList()
	{
		super();
	}

	public QueueRecordsList( Collection<NodeBlock> col )
	{
		super( col );
	}

	public QueueRecordsList( int initialCapacity )
	{
		super( initialCapacity );
	}

	public void add( String clientId, NodeBlock iotBlock )
	{
		if (recordsMap == null) {
			recordsMap = new HashMap<String, NodeBlock>();
		}

		recordsMap.put( clientId, iotBlock );
	}

	public String toString()
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.startList( HEADER );
		addRecordsObject( writer );
		writer.endList();
		writer.endDocument();

		return writer.toString();
	}


	private void addRecordsObject( JSONWriter writer )
	{
		writer.startObject();
		addBodyObject( writer );
		writer.endObject();
	}

	private void addBodyObject( JSONWriter writer )
	{
		writer.startObject( "body" );
		addDataList( writer );
		writer.endObject();
	}

	private void addDataList( JSONWriter writer )
	{
		writer.startList( "data" );

		NodeBlock block;
		Iterator<?> it;
		String clientId = null;
		boolean recordsFormat = (recordsMap != null);
		if (recordsFormat) {
			it = recordsMap.keySet().iterator();
		}
		else {
			it = iterator();
			clientId = Encoder.generateKey( 12 ); // random 12 character identifier
		}
		while (it.hasNext()) {
			if (recordsFormat) {
				clientId = (String) it.next();
				block = recordsMap.get( clientId );
			}
			else {
				block = (NodeBlock) it.next();
			}
			writer.addElement( new String(block.toEventObject(clientId, DataFormat.JSON)) );
		}

		writer.endList();
	}


	private HashMap<String, NodeBlock> recordsMap;
}
