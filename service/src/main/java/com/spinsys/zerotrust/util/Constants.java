package com.spinsys.zerotrust.util;

import java.text.SimpleDateFormat;

public interface Constants
{
	public static enum Parameter {
		CONFIG_FILE( "config", "--config" ),
		AWS_PROFILE( "aws.profile", "--profile" ),
		AWS_ACCESS_KEY( "aws.accessKey", "--accessKey" ),
		AWS_SECRET_KEY( "aws.secretKey", "--secretKey" ),
		AWS_SESSION_TOKEN( "aws.sessionToken", "--sessionToken" ),
		AWS_REGION( "aws.region", "--region" ),
		AWS_ACTION_TYPE( "aws.action", "--actionType" ),
		S3_BUCKET_NAME( "s3.bucket", "--bucketName" ),
		S3_OBJECT_NAME( "s3.object", "--objectName" ),
		S3_SELECT_KEY( "s3.select.key", "--statementKey" ),
		S3_HOST_ADDRESS( "s3.host.address", "--s3Address" ),
		BROKER_TEST_MODE( "broker.testMode", "--testMode" ),
		BROKER_VAULT_ADDRESS( "broker.vaultAddress", "--vaultAddress" ),
		BROKER_VAULT_KEY( "broker.vaultKey", "--vaultKey" ),
		BROKER_VAULT_TOKEN( "broker.vaultToken", "--vaultToken" ),
		BROKER_DBMS_TYPE( "broker.dbms.type", "--dbmsType" ),
		BROKER_DB_ADDRESS( "broker.dynamoAddress", "--ddbAddress" ),
		BROKER_JDBC_CONFIG( "broker.jdbc.config", "--jdbcConfig" ),
		CLIENT_SERVICE_URL( "client.broker.server", "--brokerServer" ),
		CLIENT_SERVICE_TYPE( "client.broker.type", "--brokerType" ),
		CLIENT_QUEUE_URL( "client.queue.server", "--queueServer" ),
		CLIENT_QUEUE_TYPE( "client.queue.type", "--queueType" ),
		CLIENT_QUEUE_NAME( "client.queue.name", "--queueName" ),
		CLIENT_SEND_INTERVAL( "client.message.interval", "--messageInterval" ),
		CLIENT_OP_COUNT( "client.op.count", "--count" ),
		CLIENT_OUTPUT_FORMAT( "client.message.format", "--format" ),
		CLIENT_OUTPUT_PATH( "client.output.path", "--output" ),
		SECURITY_TRUSTSTORE_PATH( "security.truststore.path", "--trustStore" ),
		SECURITY_TRUSTSTORE_PASS( "security.truststore.password", "--trustStorePassword" ),
		SECURITY_TRUSTSTORE_TYPE( "security.truststore.type", "--trustStoreType" ),
		SECURITY_KEYSTORE_PATH( "security.keystore.path", "--keyStore" ),
		SECURITY_KEYSTORE_PASS( "security.keystore.password", "--keyStorePassword" ),
		SECURITY_KEYSTORE_TYPE( "security.keystore.type", "--keyStoreType" );

		private Parameter( String property, String argument )
		{
			this.property = property;
			this.argument = argument;
		}

		public String toString()
		{
			return new StringBuilder( name() ).append( "{" ).
				append( "property: " ).append( property ).append( ", " ).
				append( "argument: " ).append( argument ).append( "}" ).
				toString();
		}

		public final String property, argument;
	}

	public static enum BrokerRequest {
		VALIDATE_IOT( "/validate", "POST" ),
		DECRYPT_IOT( "/decrypt", "POST" ),
		SECURE_KEYS( "/secure", "PUT" ),
		UPDATE_CUSTOMER( "/update", "POST" );

		public String getRequestPath() {
			return path;
		}

		public String getRequestMethod() {
			return method;
		}

		private BrokerRequest( String path, String method ) {
			this.path = path;
			this.method = method;
		}

		private final String path;
		private final String method;
	}

	public static enum BrokerType {
		HTTP, LAMBDA;
	}

	public static enum QueueType {
		SQS, MQ, MQTT;
	}

	public static enum AWSClientType {
		DYNAMODB, EC2, LAMBDA, S3, SQS;
	}

	public static enum S3Action {
		DELETE, GET, LIST, PUT, SELECT;
	}

	public static enum DataFormat {
		AVRO( "application/avro", ".avro" ),
		JSON( "application/json", ".json" ),
		XML( "application/xml", ".xml" ),
		TEXT( "text/plain", ".txt" ),
		TABLE( "text/csv", ".csv" );
		
		public String getMimeType() { return mimeType; }
		public String getSuffix() { return suffix; }
		
		private DataFormat( String mimeType, String suffix ) {
			this.mimeType = mimeType;
			this.suffix = suffix;
		}
		
		private final String mimeType;
		private final String suffix;
	}

	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat( "yyyy-MM-dd" );
	public final static SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat( "MM-dd-yyyy HH:mm:ss" );
	public final static SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat( "MMddyyyyHHmmss" );
	
	public final static String AWS_ACCESS_KEY_ID_PROPERTY = "aws.accessKeyId";
	public final static String AWS_SECRET_ACCESS_KEY_PROPERTY = "aws.secretKey";

	public static final String BROKER_AUTH_TOKEN_KEY = "broker.authToken";

	public static final String S3_SELECT_KEY_TMPL = "s3.select.%s.stmt";

	public static final String DEFAULT_AWS_REGION = "us-east-1";
	public static final String DEFAULT_BROKER_URL = "http://localhost:8080";
	public static final String DEFAULT_QUEUE_NAME = "mdaca/zerotrust";
	public static final String DEFAULT_SECRETS_TYPE = "PKCS12";
	public static final int DEFAULT_MESSAGE_INTERVAL = 60;
	public static final int DEFAULT_OP_COUNT = 1;

	public static final String APPLICATION_MODE_KEY = "applicationMode";
	public static final String APPLICATION_MODE_BROKER = "broker";
	public static final String APPLICATION_MODE_DEVICE = "device";
}
