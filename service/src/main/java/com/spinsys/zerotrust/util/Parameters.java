package com.spinsys.zerotrust.util;

import com.spinsys.zerotrust.client.BrokerPayload;
import com.veetechis.lib.io.FileUtils;
import com.veetechis.lib.preferences.ArgumentsParser;
import com.veetechis.lib.util.KeyValueList;
import com.veetechis.lib.util.KeyValuePair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class Parameters
implements Constants
{
	/**
	 * Default configuration value indicates a value must be provided by the
	 * user.
	 */
	protected static final String USER_MUST_PROVIDE = "userMustProvide";


	/**
	 * Creates a pseudo-singleton instance with the given command-line
	 * parameters.
	 * 
	 * @throws  Exception
	 *   if a parsing error occurs or if an instance already exists.
	 */
	public static synchronized void createInstance( String[] args )
	throws Exception
	{
		instance = new Parameters( args );
	}

	/**
	 * Returns the instance or null if not yet created.
	 * 
	 * @return  the instance or null.
	 */
	public static synchronized Parameters getInstance()
	{
		return instance;
	}


	/**
	 * Creates a new instance with the given arguments.
	 * 
	 * @param args  the arguments to parse.
	 * @throws Exception
	 *   if a parsing error occurs or if an instance already exists.
	 */
	public Parameters( String[] args )
	throws Exception
	{
		if (instance != null) {
			String message = String.format( "Instance already exists: %s", instance.toString() );
			throw new InstantiationException( message );
		}

		config = new Properties();
		if (args.length > 0) {
			KeyValueList parms = new ArgumentsParser( ArgumentsParser.DEFAULT_SWITCH, "=" ).parse( args );
			if (parms.get(Parameter.CONFIG_FILE.argument) != null) {
				readConfig( parms.get(Parameter.CONFIG_FILE.argument).getValue() );
				parms.remove( Parameter.CONFIG_FILE.argument );
			}
			if (parms.get(Parameter.BROKER_TEST_MODE.argument) != null) {
				enableBrokerTestMode();
				parms.remove( Parameter.BROKER_TEST_MODE.argument );
			}
			if (parms.get(Parameter.CLIENT_SERVICE_TYPE.argument) != null) {
				setBrokerType( BrokerType.valueOf(parms.get(Parameter.CLIENT_SERVICE_TYPE.argument).getValue().toUpperCase()) );
				parms.remove( Parameter.CLIENT_SERVICE_TYPE.argument );
			}
			if (parms.get(Parameter.CLIENT_QUEUE_TYPE.argument) != null) {
				setQueueType( QueueType.valueOf(parms.get(Parameter.CLIENT_QUEUE_TYPE.argument).getValue().toUpperCase()) );
				parms.remove( Parameter.CLIENT_QUEUE_TYPE.argument );
			}
			if (parms.get(Parameter.CLIENT_SEND_INTERVAL.argument) != null) {
				setMessageInterval( Integer.parseInt(parms.get(Parameter.CLIENT_SEND_INTERVAL.argument).getValue()) );
				parms.remove( Parameter.CLIENT_SEND_INTERVAL.argument );
			}
			if (parms.get(Parameter.CLIENT_OP_COUNT.argument) != null) {
				setOperationCount( Integer.parseInt(parms.get(Parameter.CLIENT_OP_COUNT.argument).getValue()) );
				parms.remove( Parameter.CLIENT_OP_COUNT.argument );
			}
			if (parms.get(Parameter.CLIENT_OUTPUT_FORMAT.argument) != null) {
				setOutputFormat( DataFormat.valueOf(parms.get(Parameter.CLIENT_OUTPUT_FORMAT.argument).getValue().toUpperCase()) );
				parms.remove( Parameter.CLIENT_OUTPUT_FORMAT.argument );
			}

			if (parms.size() > 0) {
				Parse:
				for (KeyValuePair kvp : parms.toArray()) {
					for (Parameter param : Parameter.values()) {
						if (param.argument.equals(kvp.getKey())) {
							config.setProperty( param.property, kvp.getValue() );
							parms.remove( param.argument );
							break Parse;
						}
					}
				}
			}
		}
	}


	public void setAWSSecurityProfile( String profile )
	{
		config.setProperty( Parameter.AWS_PROFILE.property, profile );
	}

	public String getAWSSecurityProfile()
	{
		return config.getProperty( Parameter.AWS_PROFILE.property );
	}

	public void setAWSAccessKey( String accessKey )
	{
		config.setProperty( Parameter.AWS_ACCESS_KEY.property, accessKey );
	}

	public String getAWSAccessKey()
	{
		return config.getProperty( Parameter.AWS_ACCESS_KEY.property );
	}

	public void setAWSSecretKey( String secretKey )
	{
		config.setProperty( Parameter.AWS_SECRET_KEY.property, secretKey );
	}

	public String getAWSSecretKey()
	{
		return config.getProperty( Parameter.AWS_SECRET_KEY.property );
	}

	public boolean hasAWSConfigCredentials()
	{
		return (getAWSAccessKey() != null && getAWSSecretKey() != null);
	}

	public void setAWSSessionToken( String token )
	{
		config.setProperty( Parameter.AWS_SESSION_TOKEN.property, token );
	}

	public String getAWSSessionToken()
	{
		return config.getProperty( Parameter.AWS_SESSION_TOKEN.property );
	}

	public boolean hasAWSSessionToken()
	{
		return (getAWSSessionToken() != null);
	}

	public void setAWSRegion( String region )
	{
		config.setProperty( Parameter.AWS_REGION.property, region );
	}

	public String getAWSRegion()
	{
		return config.getProperty( Parameter.AWS_REGION.property );
	}

	public void setAWSAction( String action )
	{
		config.setProperty( Parameter.AWS_ACTION_TYPE.property, action.toUpperCase() );
	}

	public String getAWSAction()
	{
		return config.getProperty( Parameter.AWS_ACTION_TYPE.property ).toUpperCase();
	}

	public boolean doS3Operation()
	{
		String action = getAWSAction();
		return (action != null && S3Action.valueOf(action) != null && getS3BucketName() != null);
	}

	public boolean doUploadToS3()
	{
		return (doS3Operation() && getAWSAction().equals(S3Action.PUT.name()));
	}

	public void setS3BucketName( String bucketName )
	{
		config.setProperty( Parameter.S3_BUCKET_NAME.property, bucketName );
	}

	public String getS3BucketName()
	{
		return config.getProperty( Parameter.S3_BUCKET_NAME.property );
	}

	public void setS3ObjectName( String objectName )
	{
		if (objectName == null) {
			config.remove( Parameter.S3_OBJECT_NAME.property );
		}
		else {
			config.setProperty( Parameter.S3_OBJECT_NAME.property, objectName );
		}
	}

	public String getS3ObjectName()
	{
		return config.getProperty( Parameter.S3_OBJECT_NAME.property );
	}

	public void setS3SelectKey( String key )
	{
		config.setProperty( Parameter.S3_SELECT_KEY.property, key );
	}

	public String getS3SelectKey()
	{
		return config.getProperty( Parameter.S3_SELECT_KEY.property );
	}

	public String getS3SelectStatement()
	{
		return config.getProperty( String.format(S3_SELECT_KEY_TMPL, getS3SelectKey()) );
	}

	public void setS3Address( String address )
	{
		config.setProperty( Parameter.S3_HOST_ADDRESS.property, address );
	}

	public String getS3Address()
	{
		return config.getProperty( Parameter.S3_HOST_ADDRESS.property );
	}

	public boolean hasS3Address()
	{
		return (getS3Address() != null);
	}

	public boolean doQueueOperation()
	{
		// TODO: real logic
		return (!doS3Operation());
	}

	public void enableBrokerTestMode()
	{
		config.setProperty( Parameter.BROKER_TEST_MODE.property, String.valueOf(Boolean.TRUE) );
	}

	public boolean isBrokerTestMode()
	{
		return Boolean.parseBoolean( config.getProperty(Parameter.BROKER_TEST_MODE.property) );
	}

	public void setBrokerAuthenticationToken( String token )
	{
		config.setProperty( BROKER_AUTH_TOKEN_KEY, token );
	}

	public String getBrokerAuthenticationToken()
	{
		return config.getProperty( BROKER_AUTH_TOKEN_KEY );
	}

	public void setBrokerVaultAddress( String address )
	{
		config.setProperty( Parameter.BROKER_VAULT_ADDRESS.property, address );
	}

	public String getBrokerVaultAddress()
	{
		return config.getProperty( Parameter.BROKER_VAULT_ADDRESS.property );
	}

	public void setBrokerVaultKey( String key )
	{
		config.setProperty( Parameter.BROKER_VAULT_KEY.property, key );
	}

	public String getBrokerVaultKey()
	{
		return config.getProperty( Parameter.BROKER_VAULT_KEY.property );
	}

	public void setBrokerVaultToken( String token )
	{
		config.setProperty( Parameter.BROKER_VAULT_TOKEN.property, token );
	}

	public String getBrokerVaultToken()
	{
		return config.getProperty( Parameter.BROKER_VAULT_TOKEN.property );
	}

	public void setDynamoAddress( String address )
	{
		config.setProperty( Parameter.BROKER_DB_ADDRESS.property, address );
	}

	public String getDynamoAddress()
	{
		return config.getProperty( Parameter.BROKER_DB_ADDRESS.property );
	}

	public void setBrokerAddress( String address )
	{
		config.setProperty( Parameter.CLIENT_SERVICE_URL.property, address );
	}

	public String getBrokerAddress()
	{
		return config.getProperty( Parameter.CLIENT_SERVICE_URL.property );
	}

	public void setBrokerType( BrokerType type )
	{
		config.setProperty( Parameter.CLIENT_SERVICE_TYPE.property, type.name() );
	}

	public BrokerType getBrokerType()
	{
		return BrokerType.valueOf( config.getProperty(Parameter.CLIENT_SERVICE_TYPE.property) );
	}

	public void setBrokerRequestType( String requesterId, BrokerRequest request )
	{
		brokerRequestMap.put( requesterId, request );
	}

	public BrokerRequest getBrokerRequestType( String requesterId )
	{
		return brokerRequestMap.get( requesterId );
	}

	public void setBrokerRequestPayload( String requesterId, BrokerPayload payload )
	{
		brokerPayloadMap.put( requesterId, payload );
	}

	public BrokerPayload getBrokerRequestPayload( String requesterId )
	{
		return brokerPayloadMap.get( requesterId );
	}

	public void setQueueServerAddress( String address )
	{
		config.setProperty( Parameter.CLIENT_QUEUE_URL.property, address );
	}

	public String getQueueServerAddress()
	{
		return config.getProperty( Parameter.CLIENT_QUEUE_URL.property );
	}

	public void setQueueType( QueueType type )
	{
		config.setProperty( Parameter.CLIENT_QUEUE_TYPE.property, type.name() );
	}

	public QueueType getQueueType()
	{
		return QueueType.valueOf( config.getProperty(Parameter.CLIENT_QUEUE_TYPE.property) );
	}

	public void setQueueName( String name )
	{
		config.setProperty( Parameter.CLIENT_QUEUE_NAME.property, name );
	}

	public String getQueueName()
	{
		return config.getProperty( Parameter.CLIENT_QUEUE_NAME.property );
	}

	public boolean doSendToRouter()
	{
		return (getQueueServerAddress() != null && getQueueType() != null && getQueueName() != null);
	}

	public void setMessageInterval( int interval )
	{
		config.setProperty( Parameter.CLIENT_SEND_INTERVAL.property, String.valueOf(interval) );
	}

	public int getMessageInterval()
	{
		return Integer.parseInt( config.getProperty(Parameter.CLIENT_SEND_INTERVAL.property) );
	}

	public void setOperationCount( int count )
	{
		config.setProperty( Parameter.CLIENT_OP_COUNT.property, String.valueOf(count) );
	}

	public int getOperationCount()
	{
		int count = DEFAULT_OP_COUNT;
		if ( config.getProperty(Parameter.CLIENT_OP_COUNT.property) != null ) {
			count = Integer.parseInt( config.getProperty(Parameter.CLIENT_OP_COUNT.property) );
		}

		return count;
	}

	public boolean hasMessageBroker()
	{
		return (getQueueServerAddress() != null && getQueueType() != null && getQueueName() != null);
	}
	
	public boolean hasBrokerService()
	{
		return (getBrokerAddress() != null && getBrokerType() != null);
	}

	public void setOutputPath( String path )
	{
		config.setProperty( Parameter.CLIENT_OUTPUT_PATH.property, path );
	}

	public String getOutputPath()
	{
		return config.getProperty( Parameter.CLIENT_OUTPUT_PATH.property );
	}

	public boolean hasOutputPath()
	{
		return (getOutputPath() != null);
	}
	
	public void setOutputFormat( DataFormat format )
	{
		config.setProperty( Parameter.CLIENT_OUTPUT_FORMAT.property, format.name() );
	}

	public DataFormat getOutputFormat()
	{
		DataFormat format = DataFormat.JSON;
		if (config.getProperty(Parameter.CLIENT_OUTPUT_FORMAT.property) != null) {
			format = DataFormat.valueOf( config.getProperty(Parameter.CLIENT_OUTPUT_FORMAT.property) );
		}

		return format;
	}

	public void setInputFile( File inputFile )
	{
		this.inputFile = inputFile;
	}

	public File getInputFile()
	{
		return inputFile;
	}

	public void setUploadFile( File uploadFile )
	{
		this.uploadFile = uploadFile;
	}

	public File getUploadFile()
	{
		return uploadFile;
	}

	public void setTruststorePath( String path )
	{
		config.setProperty( Parameter.SECURITY_TRUSTSTORE_PATH.property, path );
	}

	public String getTruststorePath()
	{
		return config.getProperty( Parameter.SECURITY_TRUSTSTORE_PATH.property );
	}

	public void setTruststorePassword( String pass )
	{
		config.setProperty( Parameter.SECURITY_TRUSTSTORE_PASS.property, pass );
	}

	public String getTruststorePassword()
	{
		return config.getProperty( Parameter.SECURITY_TRUSTSTORE_PASS.property );
	}

	public void setTruststoreType( String type )
	{
		config.setProperty( Parameter.SECURITY_TRUSTSTORE_TYPE.property, type );
	}

	public String getTruststoreType()
	{
		return config.getProperty( Parameter.SECURITY_TRUSTSTORE_TYPE.property ).toUpperCase();
	}

	public boolean hasTruststore()
	{
		return (getTruststorePath() != null);
	}

	public void setKeystorePath( String path )
	{
		config.setProperty( Parameter.SECURITY_KEYSTORE_PATH.property, path );
	}

	public String getKeystorePath()
	{
		return config.getProperty( Parameter.SECURITY_KEYSTORE_PATH.property );
	}

	public void setKeystorePassword( String pass )
	{
		config.setProperty( Parameter.SECURITY_KEYSTORE_PASS.property, pass );
	}

	public String getKeystorePassword()
	{
		return config.getProperty( Parameter.SECURITY_KEYSTORE_PASS.property );
	}

	public void setKeystoreType( String type )
	{
		config.setProperty( Parameter.SECURITY_KEYSTORE_TYPE.property, type );
	}

	public String getKeystoreType()
	{
		return config.getProperty( Parameter.SECURITY_KEYSTORE_TYPE.property ).toUpperCase();
	}

	public boolean hasKeystore()
	{
		return (getTruststorePath() != null);
	}

	public boolean isSecurityEnabled()
	{
		return (hasTruststore() && hasKeystore());
	}

	public String toString()
	{
		boolean isTestMode = isBrokerTestMode();
		String token = getAWSSessionToken();
		StringBuilder out = new StringBuilder( "[AWS: " ).
			append( "Profile = " ).append( getAWSSecurityProfile() ).
			append( "; Access Key = " ).append( getAWSAccessKey() ).
			append( "; Secret Key = " ).append( getAWSSecretKey() );
		if (token != null) {
			out.append( "; Session Token = " ).append( String.format("%s...", token.substring(0, 20)) );
		}
		out.append( "; AWS Region = " ).append( getAWSRegion() ).
			append( "; Action Type = " ).append( getAWSAction() );

		if (doS3Operation()) {
			out.append( "]\n[S3: " ).
				append( "; S3 Bucket = " ).append( getS3BucketName() ).
				append( "; S3 Object = " ).append( getS3ObjectName() );
			if (getS3SelectKey() != null) {
				out.append( "; S3 Select = " ).append( getS3SelectKey() );
			}
			if (getS3Address() != null) {
				out.append( "; S3 Host = " ).append( getS3Address() );
			}
		}

		out.append( "]\n[Broker: " ).
			append( "Test Mode = " ).append( isTestMode );
		if (!isTestMode) {
			out.append( "; Vault Address = " ).append( getBrokerVaultAddress() ).
				append( "; Vault Key = " ).append( getBrokerVaultKey() ).
				append( "; Vault Token = " ).append( getBrokerVaultToken() );
		}
		//out.append( "; DynamoDB Address = " ).append( getDynamoAddress() );
		
		out.append( "]\n[Client: " );
		if (hasBrokerService()) {
			out.append( "Broker URL = " ).append( getBrokerAddress() ).
				append( "; Broker Type = " ).append( getBrokerType().name() );
		}
		if (hasMessageBroker()) {
			if (hasBrokerService()) {
				out.append( "; " );
			}
			out.append( "Queue URL = " ).append( getQueueServerAddress() ).
				append( "; Queue Type = " ).append( getQueueType().name() ).
				append( "; Queue Name = " ).append( getQueueName() ).
				append( "; Message Interval = " ).append( String.valueOf(getMessageInterval()) );
		}
		if (hasBrokerService() || hasMessageBroker()) {
			out.append( "; " );
		}
		out.append( "Operation count = " ).append( getOperationCount() ).
			append( "; Message format = " ).append( getOutputFormat() );
		if (hasOutputPath()) {
			out.append( "; Output path = " ).append( getOutputPath() );
		}
		out.append( "]" );

		if (isSecurityEnabled()) {
			out.append( "\n[Security: " ).
				append( "Trust store path = " ).append( getTruststorePath() ).
				append( "; Trust store password = " );
			String pass = String.format( "%d*", getTruststorePassword().length() );
			out.append( pass ).
				append( "; Trust store type = " ).append( getTruststoreType() ).
				append( "; Key store path = " ).append( getKeystorePath() ).
				append( "; Key store pass = " );
			pass = String.format( "%d*", getKeystorePassword().length() );
			out.append( pass ).
				append( "; Key store type = " ).append( getKeystoreType() ).
				append( "]" );
		}
		return out.toString();
	}


	public static String getUsage()
	{
		return "Usage parameters:\n" +
			"\t--config=<configuration properties file>\n" +
			"\n\tAWS Global -\n" +
			"\t--profile=<AWS security profile>\n" +
			"\t--accesskey=<account access key ID>\n" +
			"\t--secretkey=<account secret access key>\n" +
			"\t--sessiontoken=<temporary session token>\n" +
			"\t--region=<account region [e.g. us-east-1]>\n" +
			"\t--actionType=<AWS API action [see README]>\n" +
			"\n\tAWS S3 -\n" +
			"\t--bucketName=<S3 bucket name>\n" +
			"\t--objectName=<S3 object name>\n" +
			"\t--statementKey=<S3 content select statement key>\n" +
			"\t--s3Address=<S3 endpoint URL>\n" +
			"\n\tBroker Server -\n" +
			"\t--testMode (service is test service)\n" +
			"\t--vaultAddress=<endpoint URL> (URL of HC Vault server)\n" +
			"\t--vaultToken=<token> (optional HC Vault client token)\n" +
			"\t--vaultKey=<unseal key> (HC Vault unseal key)\n" +
			"\t--ddbAddress=<endpoint URL> (URL of AWS DynamoDB local instance)\n" +
			"\n\tDevice Simulator -\n" +
			"\t--brokerServer=<host URL|function name>\n" +
			"\t--brokerType=<HTTP|LAMBDA>\n" +
			"\t--queueServer=<server URL> (URL of SQS|MQ|MQTT server)\n" +
			"\t--queueType=<SQS|MQ|MQTT>\n" +
			"\t--queueName=<queue name or topic>\n" +
			"\t--messageInterval=<message send interval (secs)>\n" +
			"\t--count=<message send interval (secs)>\n" +
			"\t--output=<number of client operations to perform [e.g. put node blocks]>\n" +
			"\t--format=<AVRO|JSON*|TABLE|TEXT|XML (* = default)>\n" +
			"\n\tSecurity -\n" +
			"\t--trustStore=<path to truststore file>\n" +
			"\t--trustStorePassword=<truststore file password>\n" +
			"\t--trustStoreType=<JKS|JCEKS|PKCS12*|PKCS11|DKS (* = default)>\n" +
			"\t--keyStore=<path to keystore file>\n" +
			"\t--keyStorePassword=<keystore file password>\n" +
			"\t--keyStoreType=<JKS|JCEKS|PKCS12*|PKCS11|DKS (* = default)>\n";
	}
	

	/**
	 * Validates the value of the specified configuration property. If the
	 * given defaultValue is null and the property doesn't have a user
	 * assigned value (="userMustProvide") then the property is removed,
	 * otherwise, the property is assigned defaultValue. 
	 * 
	 * @param config  the configuration properties.
	 * @param key  the property to validate or set.
	 * @param defaultValue  the default value to assign.
	 * @return  true if the property has a user value assigned.
	 */
	protected boolean validate( Properties config, String key, String defaultValue )
	{
		boolean userAssigned = false;

		String value = (String) config.remove( key );
		if (value == null || value.equalsIgnoreCase(USER_MUST_PROVIDE)) {
			value = defaultValue;
		}
		else {
			userAssigned = true;
		}
		if (value != null) {
			config.setProperty( key, value );
		}

		return userAssigned;
	}


	private void readConfig( String configPath )
	throws IOException
	{
		File pfile = new File( configPath );
		try {
			if (FileUtils.isReadableFile(pfile)) {
				config.load( new FileInputStream(pfile) );
				validateConfig();
			}
			else {
				if (LOG.isWarnEnabled()) {
					LOG.warn( String.format("Parameters file '%s' not readable, input ignored.", pfile) );
				}
			}
		}
		catch (FileNotFoundException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn( String.format("Parameters file '%s' not found, input ignored.", pfile) );
			}
		}
	}

	private void validateConfig()
	{
		String NO_DEFAULT = null;
		validate( config, Parameter.AWS_PROFILE.property, NO_DEFAULT );
		validate( config, Parameter.AWS_ACCESS_KEY.property, NO_DEFAULT );
		validate( config, Parameter.AWS_SECRET_KEY.property, NO_DEFAULT );
		validate( config, Parameter.AWS_SESSION_TOKEN.property, NO_DEFAULT );
		validate( config, Parameter.AWS_REGION.property, DEFAULT_AWS_REGION );
		validate( config, Parameter.AWS_ACTION_TYPE.property, NO_DEFAULT );
		validate( config, Parameter.S3_BUCKET_NAME.property, NO_DEFAULT );
		validate( config, Parameter.S3_OBJECT_NAME.property, NO_DEFAULT );
		validate( config, Parameter.S3_HOST_ADDRESS.property, NO_DEFAULT );
		validate( config, Parameter.BROKER_TEST_MODE.property, Boolean.FALSE.toString() );
		validate( config, Parameter.BROKER_VAULT_ADDRESS.property, NO_DEFAULT );
		validate( config, Parameter.BROKER_VAULT_KEY.property, NO_DEFAULT );
		validate( config, Parameter.BROKER_VAULT_TOKEN.property, NO_DEFAULT );
		validate( config, Parameter.BROKER_DB_ADDRESS.property, NO_DEFAULT );
		validate( config, Parameter.CLIENT_SERVICE_URL.property, DEFAULT_BROKER_URL );
		validate( config, Parameter.CLIENT_SERVICE_TYPE.property, BrokerType.HTTP.name() );
		validate( config, Parameter.CLIENT_QUEUE_URL.property, NO_DEFAULT );
		validate( config, Parameter.CLIENT_QUEUE_TYPE.property, QueueType.SQS.name() );
		validate( config, Parameter.CLIENT_QUEUE_NAME.property, DEFAULT_QUEUE_NAME );
		validate( config, Parameter.CLIENT_SEND_INTERVAL.property, String.valueOf(DEFAULT_MESSAGE_INTERVAL) );
		validate( config, Parameter.CLIENT_OP_COUNT.property, String.valueOf(DEFAULT_OP_COUNT) );
		validate( config, Parameter.CLIENT_OUTPUT_FORMAT.property, DataFormat.JSON.name() );
		validate( config, Parameter.SECURITY_TRUSTSTORE_PATH.property, NO_DEFAULT );
		validate( config, Parameter.SECURITY_TRUSTSTORE_PASS.property, NO_DEFAULT );
		validate( config, Parameter.SECURITY_TRUSTSTORE_TYPE.property, DEFAULT_SECRETS_TYPE );
		validate( config, Parameter.SECURITY_KEYSTORE_PATH.property, NO_DEFAULT );
		validate( config, Parameter.SECURITY_KEYSTORE_PASS.property, NO_DEFAULT );
		validate( config, Parameter.SECURITY_KEYSTORE_TYPE.property, DEFAULT_SECRETS_TYPE );
	}


	private Properties config;
	private File inputFile;
	private File uploadFile;

	private Hashtable<String,BrokerRequest> brokerRequestMap = new Hashtable<String,BrokerRequest>();
	private Hashtable<String,BrokerPayload> brokerPayloadMap = new Hashtable<String,BrokerPayload>();
	
	private static Parameters instance;

	private static final Log LOG = LogFactory.getLog( Parameters.class );
}
