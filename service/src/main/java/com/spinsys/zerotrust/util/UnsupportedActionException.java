package com.spinsys.zerotrust.util;


/**
 * Thrown to indicate an unknown or unsupported broker action is requested.
 * 
 * @author acook@spinsys.com
 */
public class UnsupportedActionException
extends UnsupportedOperationException
{
	public UnsupportedActionException( String message )
	{
		super( message );
	}
	
	public UnsupportedActionException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
