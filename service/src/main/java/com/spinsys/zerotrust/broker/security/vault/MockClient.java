package com.spinsys.zerotrust.broker.security.vault;

import java.util.HashMap;

public class MockClient
extends VaultClient
{
	public synchronized VaultBox empty( String path )
	{
		VaultBox box = storage.remove( path );
		if (box != null) {
			box.setLeaseDuration( -1 );
			box.setLeaseID( "NONE" );
			box.setRenewable( false );
		}

		return box;
	}

	public synchronized String[] inventory( String path )
	{
		String[] ids = null;

		VaultBox box = storage.get( path );
		if (box != null) {
			ids = box.getSecretIDs();
		}

		return ids;
	}

	public synchronized VaultBox open( String path )
	{
		VaultBox box = storage.get( path );
		LOG.info( String.format("Storage: %s", storage) );
		LOG.info( String.format("Returning box %s from path %s.", box, path) );
		return box;
		//return storage.get( path );
	}

	public synchronized void store( VaultBox secrets )
	{
		storage.put( secrets.getStoragePath(), secrets );
		LOG.info( String.format("Stored box %s to path %s.", secrets, secrets.getStoragePath()) );
		LOG.info( String.format("Storage: %s", storage) );
	}


	protected MockClient()
	{
		storage = new HashMap<String, VaultBox>();
	}


	private final HashMap<String, VaultBox> storage;
}
