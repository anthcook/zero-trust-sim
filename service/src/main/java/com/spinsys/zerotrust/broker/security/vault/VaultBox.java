package com.spinsys.zerotrust.broker.security.vault;

import java.util.HashMap;
import java.util.Map;

public class VaultBox
{
	public VaultBox( String path )
	{
		this.path = path;
		secrets = new HashMap<String,String>();
	}
	

	public String getStoragePath()
	{
		return path;
	}

	public boolean isLeaseRenewable()
	{
		return renewable;
	}

	public long getLeaseDuration()
	{
		return duration;
	}

	public String getLeaseID()
	{
		return id;
	}

	public String getSecret( String secretId )
	{
		return getSecrets().get( secretId );
	}

	public String[] getSecretIDs()
	{
		String[] ids = null;

		if (getSecrets().keySet() != null) {
			ids = (String[]) getSecrets().keySet().toArray();
		}

		return ids;
	}

	public void setRenewable( boolean renewable )
	{
		this.renewable = renewable;
	}

	public void setLeaseDuration( long duration )
	{
		this.duration = duration;
	}

	public void setLeaseID( String id )
	{
		this.id = id;
	}

	public void addSecret( String secretId, String secretValue )
	{
		getSecrets().put( secretId, secretValue );
	}


	protected Map<String,String> getSecrets()
	{
		return secrets;
	}


	private boolean renewable;
	private long duration;
	private String id;

	private final String path;
	private final Map<String,String> secrets;
}
