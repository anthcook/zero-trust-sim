package com.spinsys.zerotrust.broker.service;

import java.util.ArrayList;
import java.util.List;

public class IotPayload
{
	public List<String> getNodeBlocks()
	{
		return nodeBlocks;
	}

	public void setNodeBlocks( List<String> nodeBlocks )
	{
		this.nodeBlocks.clear();
		this.nodeBlocks.addAll( nodeBlocks );
	}

	
	private final List<String> nodeBlocks = new ArrayList<String>();
}