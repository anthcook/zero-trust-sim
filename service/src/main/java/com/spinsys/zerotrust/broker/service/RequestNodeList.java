package com.spinsys.zerotrust.broker.service;

import java.util.ArrayList;
import java.util.List;

public class RequestNodeList
{
	public void addNodeBlock( String block )
	{
		nodeBlocks.add( block );
	}

	public void addNodeBlocks( List<String> blockList )
	{
		nodeBlocks.addAll( blockList );
	}

	public void setNodeBlocks( List<String> blockList )
	{
		nodeBlocks.clear();
		nodeBlocks.addAll( blockList );
	}

	public List<String> getNodeBlocks()
	{
		return nodeBlocks;
	}

	
	private List<String> nodeBlocks = new ArrayList<String>();
}
