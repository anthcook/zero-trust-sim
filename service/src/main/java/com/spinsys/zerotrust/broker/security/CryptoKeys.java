package com.spinsys.zerotrust.broker.security;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class CryptoKeys
{
	public CryptoKeys( PrivateKey privateKey, PublicKey publicKey, SecretKey symmetricKey, IvParameterSpec initVector )
	{
		this.privateKey = privateKey;
		this.publicKey = publicKey;
		this.symmetricKey = symmetricKey;
		this.initVector = initVector;
	}
	

	public final PrivateKey privateKey;
	public final PublicKey publicKey;
	public final SecretKey symmetricKey;
	public final IvParameterSpec initVector;
}
