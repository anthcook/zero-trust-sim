package com.spinsys.zerotrust.broker.db;

public class Customer
{
	public Customer( String deviceId, String customerId, String accountId )
	{
		this.deviceId = deviceId;
		this.customerId = customerId;
		this.accountId = accountId;
	}
	
	public String getDeviceID()
	{
		return deviceId;
	}

	public String getCustomerID()
	{
		return customerId;
	}

	public String getAccountID()
	{
		return accountId;
	}


	public final String deviceId;
	public final String customerId;
	public final String accountId;
}
