package com.spinsys.zerotrust.broker.service;

import java.util.Base64;
import java.util.Iterator;

import com.spinsys.util.Utils;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.spinsys.zerotrust.broker.security.vault.VaultBox;
import com.spinsys.zerotrust.broker.security.vault.VaultClient;
import com.spinsys.zerotrust.broker.security.vault.VaultKeys;
//import com.spinsys.aws.web.api.AWSImpl;
//import com.spinsys.aws.web.factory.EC2ImplFactory;
import com.spinsys.zerotrust.broker.service.VaultBrokerController;
import com.spinsys.zerotrust.io.NodeBlock;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.security.Crypter;
import com.veetechis.lib.security.Crypter.CipherStrength;
import com.veetechis.lib.text.JSONWriter;
import com.veetechis.lib.util.ArrayConverter;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * REST controller for MDACA Vitro Broker action requests.
 * 
 * @author acook@spinsys.com
 */
@RestController
public class VaultBrokerController
implements Constants
{
	@PostMapping("/validate")
	@ResponseBody
	public String validate( @RequestBody RequestNodeList nodeList )
	{
		Parameters config = Parameters.getInstance();

		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.startList( "results" );
		Iterator<String> it = nodeList.getNodeBlocks().iterator();
		NodeBlock iotBlock;
		String iotBlockId;
		String iotBlockTimestamp = null;
		String iotBlockHash = null;
		boolean iotValid = false;
		while( it.hasNext() ) {
			try {	// TODO: parse errors
				iotBlock = NodeBlock.getInstance( Base64.getDecoder().decode(it.next()) );
				Utils.dumpData( iotBlock );
				iotBlockId = Hex.encodeHexString( iotBlock.getID() );
				iotBlockTimestamp = Long.toString( (long)ArrayConverter.bytesToInt(iotBlock.getTimestamp()) );
				iotBlockHash = Hex.encodeHexString( iotBlock.getDigest() );
				String storagePath = VaultClient.getStoragePath( iotBlockId );
				//VaultClient vc = VaultClient.getInstance( config );
				//VaultBox box = vc.open( storagePath );
				VaultBox box = VaultClient.getInstance( config ).open( storagePath );
				LOG.info( String.format("Using client box %s.", box) );
				if (box == null) {
					// TODO: 'not found'
				}
				else {
					byte[] hashBytes = iotBlock.toDigestRecord();
					LOG.info( String.format("Node bytes: %s", Hex.encodeHexString(hashBytes)) );
					byte[] blockHash = Crypter.digest( CipherStrength.SHA_MEDIUM, iotBlock.toDigestRecord() );
					String testHash = Hex.encodeHexString( blockHash );
					LOG.info( String.format("Validating block: %s", iotBlockId) );
					LOG.info( String.format("Block digest: %s", iotBlockHash) );
					LOG.info( String.format("Test hash: %s", testHash) );
					iotValid = iotBlockHash.equals( testHash );
					//iotValid = iotBlockHash.equals( Hex.encodeHexString(blockHash) );
				}
				writer.startObject();
				writer.addStringField( "NodeId", iotBlockId );
				writer.addStringField( "Timestamp", iotBlockTimestamp );
				writer.addStringField( "Digest", iotBlockHash );
				writer.addBooleanField( "Validated", Boolean.valueOf(iotValid) );
				writer.endObject();
			}
			catch (Exception e) {
				if (LOG.isErrorEnabled()) {
					LOG.error( "IoT block parsing error.", e );
				}
			}
		}
		writer.endList();
		writer.endDocument();

		return writer.toString();
	}

	@PutMapping("/secure")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public String secure( @RequestBody VaultKeys keys )
	{
		String response = null;

		Parameters config = Parameters.getInstance();
		try {
			String nodeId = Hex.encodeHexString( keys.getNodeID() );
			String encKey = Hex.encodeHexString( keys.getEncryptionKey() );
			String sigKey = Hex.encodeHexString( keys.getSignatureKey() );
			LOG.info( String.format("Storing keys for node ID %s.", nodeId) );
			LOG.info( String.format("Encryption key (sym): %s.", encKey) );
			LOG.info( String.format("Signature key (pub): %s.", sigKey) );

			String storagePath = VaultClient.getStoragePath( nodeId );
			VaultBox box = new VaultBox( storagePath );
			box.addSecret( "encryption", encKey );
			box.addSecret( "signature", sigKey );
			VaultClient vc = VaultClient.getInstance( config );
			LOG.info( String.format("Got vault client: %s", vc) );
			vc.store( box );
			LOG.info( String.format("Stored client box %s to path %s.", box, storagePath) );
			//VaultClient.getInstance( config ).store( box );
		}
		catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( "Error storing keys in vault.", e );
			}
			// TODO: handle errors
		}

		return response;
	}


	private static final Log LOG = LogFactory.getLog( VaultBrokerController.class );
}
