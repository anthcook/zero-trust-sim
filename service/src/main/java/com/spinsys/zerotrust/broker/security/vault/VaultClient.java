package com.spinsys.zerotrust.broker.security.vault;

import com.spinsys.zerotrust.util.Parameters;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class VaultClient
{
	public synchronized static VaultClient getInstance( Parameters config )
	{
		VaultClient client;
		if (config.isBrokerTestMode()) {
			if (mockClient == null) {
				mockClient = new MockClient();
			}
			client = mockClient;
		}
		else {
			if (realClient == null) {
				realClient = new HashiClient( config );
			}
			client = realClient;
		}

		return client;
	}

	public static String getStoragePath( String nodeId )
	{
		return String.format( "kv/%s", nodeId );
	}


	public abstract VaultBox empty( String path );
	public abstract String[] inventory( String path );
	public abstract VaultBox open( String path );
	public abstract void store( VaultBox secrets );


	private static MockClient mockClient;
	private static HashiClient realClient;

	protected static final Log LOG = LogFactory.getLog( VaultClient.class );
}
