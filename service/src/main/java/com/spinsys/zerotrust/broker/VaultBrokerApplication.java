package com.spinsys.zerotrust.broker;

import javax.naming.ConfigurationException;

import com.spinsys.zerotrust.client.DeviceSim;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaultBrokerApplication
{
	public static void main( String[] args )
	{
		String application = System.getProperty(
			Constants.APPLICATION_MODE_KEY, Constants.APPLICATION_MODE_DEVICE );
			
		if (application.equals(Constants.APPLICATION_MODE_DEVICE)) {
			try {
				DeviceSim.main( args );
			}
			catch (ConfigurationException e) {
				System.out.println( Parameters.getUsage() );
			}
		}
		else {
			if (args.length > 0) {
				try {
					Parameters.createInstance( args );
				}
				catch (Exception e) {
					if (LOG.isErrorEnabled()) {
						LOG.error( "Error creating broker configuration.", e );
					}
				}
			}
			SpringApplication.run( VaultBrokerApplication.class, args );
		}
	}

	
	private static final Log LOG = LogFactory.getLog( VaultBrokerApplication.class );
}
