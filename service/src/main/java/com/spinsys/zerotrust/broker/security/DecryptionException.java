package com.spinsys.zerotrust.broker.security;

import java.security.GeneralSecurityException;

/**
 * Thrown to indicate an error while decrypting message data.
 * 
 * @author acook@spinsys.com
 */
public class DecryptionException
extends GeneralSecurityException
{
	public DecryptionException( String message )
	{
		super( message );
	}
	
	public DecryptionException( Throwable cause )
	{
		super( cause );
	}
	
	public DecryptionException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
