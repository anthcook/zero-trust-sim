package com.spinsys.zerotrust.broker.security.vault;

import java.util.Base64;

public class VaultKeys
{
	public VaultKeys( String nodeId, String encryptionKey, String signatureKey )
	{
		this.nodeId = nodeId;
		this.encryptionKey = encryptionKey;
		this.signatureKey = signatureKey;
	}

	public byte[] getNodeID()
	{
		return Base64.getDecoder().decode( nodeId );
	}

	public byte[] getEncryptionKey()
	{
		return Base64.getDecoder().decode( encryptionKey );
	}

	public byte[] getSignatureKey()
	{
		return Base64.getDecoder().decode( signatureKey );
	}

	
	private final String nodeId;
	private final String encryptionKey;
	private final String signatureKey;
}
