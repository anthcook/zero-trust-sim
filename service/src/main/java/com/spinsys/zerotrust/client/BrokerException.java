package com.spinsys.zerotrust.client;

/**
 * Thrown to indicate an error while communicating with the vault broker.
 * 
 * @author acook@spinsys.com
 */
public class BrokerException
extends Exception
{
	public BrokerException( String message )
	{
		super( message );
	}
	
	public BrokerException( Throwable cause )
	{
		super( cause );
	}
	
	public BrokerException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
