package com.spinsys.zerotrust.client;

import java.time.Instant;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.internal.security.SSLSocketFactoryFactory;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import com.spinsys.zerotrust.io.NodeBlock;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.text.JSONWriter;


/**
 * MQTTClient provides access to MQTT topics for Vitro messages.
 */
public class MQTTClient
implements MqttCallback, Constants
{
	public static enum ClientType {
		RECEIVER( "zerotrustMon" ),
		SENDER( "zerotrustSim" );

		public String getID()
		{
			return id;
		}

		ClientType( String id )
		{
			this.id = id;
		}

		private final String id;
	}

	/**
	 * Creates a new instance of the specified type.
	 */
	public MQTTClient( ClientType type, Parameters config )
	throws Exception
	{
		this.config = config;
		this.type = type;

		brokerUri = config.getQueueServerAddress();
		boolean secured = config.isSecurityEnabled();
		if (!brokerUri.toLowerCase().startsWith("ssl://")) {
			if (brokerUri.toLowerCase().startsWith("tcp://")) {
				if (secured) {
					brokerUri = String.format( "ssl://%s", brokerUri.substring(6) );
				}
			}
			else {
				if (secured) {
					brokerUri = String.format( "ssl://%s", brokerUri );
				}
				else {
					brokerUri = String.format( "tcp://%s", brokerUri );
				}
			}
		}

		String tmpDir = String.format("%s/messages", System.getProperty("java.io.tmpdir") );
		MqttDefaultFilePersistence filePersistence = new MqttDefaultFilePersistence( tmpDir );
		mqttClient = new MqttAsyncClient( brokerUri, type.getID(), filePersistence );
		
		if (LOG.isInfoEnabled()) {
			LOG.info( String.format(
				"Created client %s to connect to %s using file persistence to %s",
				type.getID(), brokerUri, tmpDir) );
		}

		if (type == ClientType.RECEIVER) {
			connect();
		}
	}

	@Override
    public void connectionLost( Throwable cause )
	{
		if (LOG.isWarnEnabled()) {
			LOG.warn( String.format("Lost connection to broker %s, reconnectin...", brokerUri), cause );
		}

		connected = false;
    }

	@Override
	public void messageArrived( String topic, MqttMessage message )
	throws MqttException
	{
		if (LOG.isInfoEnabled()) {
			LOG.info( String.format("Received message from topic '%s':\n%s", topic, message) );
		}
    }

	@Override
	public void deliveryComplete( IMqttDeliveryToken token )
	{
		if (LOG.isInfoEnabled()) {
			LOG.info( String.format("Delivery is complete for %s", Integer.valueOf(token.getMessageId())) );
		}
	}

	/**
	 * Sends the given IoT sensor block to the configured broker topic.
	 * 
	 * @throws Exception
	 */
	public String sendIotMessage( String clientId, List<NodeBlock> iotBlocks )
	throws MqttException
	{
		//{
		//  "format": "json",
		//  "payload": [
		//    {
		//      "nodeId": "01238D83EDF89F9CEE",
		//      "timestamp": "<block time seconds>"
		//      "nodeDataValue": "QppzjgKGe...",
		//    },...
		//  ],
		//  "qos": 0,
		//  "timestamp": <now seconds>,
		//  "topic": "<customer/topic>"
		//}

		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.addStringField( "format", "json" );
		writer.startList( "payload" );
		for (NodeBlock block : iotBlocks) {
			writer.addElement( "\"" + new String(block.toEventObject(clientId, DataFormat.JSON)) + "\"" );
		}
		writer.endList();
		writer.addNumberField( "qos", 0 );
		writer.addNumberField( "timestamp", Instant.now().getEpochSecond() );
		writer.addStringField( "topic", config.getQueueName() );
		writer.endDocument();
		String message = writer.toString();

		if (LOG.isInfoEnabled()) {
			LOG.info( String.format( "Sending IoT message:\n%s", message) );
		}

		if (!connected) {
			connect();
		}
		
		publishMessage( message );

		return message;
	}

	/**
	 * Connects to the MQTT server.
	 */
	public void connect()
	throws MqttException
	{
		// sensible defaults
		connectOptions = new MqttConnectOptions();
		connectOptions.setCleanSession( true );
		connectOptions.setConnectionTimeout( 60 );
		connectOptions.setKeepAliveInterval( 15 );
		// probably don't need it...
		//connectOptions.setWill( "willTopic", "willMessage".getBytes(), 0, true );
		
		if( config.isSecurityEnabled() ) {
			Properties sslProperties = new Properties();
			sslProperties.put( SSLSocketFactoryFactory.TRUSTSTORE, config.getTruststorePath() );
			sslProperties.put( SSLSocketFactoryFactory.TRUSTSTOREPWD, config.getTruststorePassword() );
			sslProperties.put( SSLSocketFactoryFactory.TRUSTSTORETYPE, config.getTruststoreType() );
			
			sslProperties.put( SSLSocketFactoryFactory.CLIENTAUTH, true );
			sslProperties.put( SSLSocketFactoryFactory.KEYSTORE, config.getKeystorePath() );
			sslProperties.put( SSLSocketFactoryFactory.KEYSTOREPWD, config.getKeystorePassword() );
			sslProperties.put( SSLSocketFactoryFactory.KEYSTORETYPE, config.getKeystoreType() );
			
			connectOptions.setSSLProperties( sslProperties );
		}

		if (type == ClientType.RECEIVER) {
			mqttClient.setCallback( this );
		}
		IMqttToken connectToken = mqttClient.connect( connectOptions );
		connectToken.waitForCompletion();
		connected = true;

		if (LOG.isInfoEnabled()) {
			LOG.info( String.format("Connected to broker %s.", brokerUri) );
		}

		if (type == ClientType.RECEIVER) {
			subscribe();
		}
	}

	/**
	 * Disconnects from the MQTT server.
	 * 
	 * @throws Exception
	 */
	public void disconnect()
	throws MqttException
	{
		if (type == ClientType.RECEIVER) {
			unsubscribe();
		}
		
		IMqttToken connectToken = mqttClient.disconnect();
		connectToken.waitForCompletion();
		connected = false;

		if (LOG.isInfoEnabled()) {
			LOG.info( String.format("Disconnected from broker %s.", brokerUri) );
		}
	}

	/**
	 * Returns true if broker client is connected.
	 */
	public boolean isConnected()
	{
		return connected;
	}


	private void subscribe()
	throws MqttException
	{
		String topic = config.getQueueName();
		IMqttToken subToken = mqttClient.subscribe( topic, 0 );
		subToken.waitForCompletion();

		if (LOG.isInfoEnabled()) {
			LOG.info( String.format("Subscribed to topic %s with qos 0.", topic) );
		}
	}

	private void unsubscribe()
	throws MqttException
	{
		String topic = config.getQueueName();
		IMqttToken subToken = mqttClient.unsubscribe( topic );
		subToken.waitForCompletion();
		
		if (LOG.isInfoEnabled()) {
			LOG.info( String.format("Unsubscribed from topic %s.", topic) );
		}
	}

	private void publishMessage( String message )
	{
		String topic = config.getQueueName();
		
		try	{
			mqttClient.publish( topic, message.getBytes(), 0, true );
		}
		catch( MqttException exc ) {
			if (LOG.isErrorEnabled()) {
				LOG.error( "Unable to publish message.", exc );
			}
		}
	}
	
	
	protected String brokerUri;
	protected ClientType type;
	protected Parameters config;
	protected MqttAsyncClient mqttClient;
	protected MqttConnectOptions connectOptions;

	private boolean connected;

	private static final Log LOG = LogFactory.getLog( MQTTClient.class );
}
