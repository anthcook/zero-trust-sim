package com.spinsys.zerotrust.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.naming.ConfigurationException;

import com.spinsys.aws.web.api.s3.S3Impl;
import com.spinsys.aws.web.factory.LambdaImplFactory;
import com.spinsys.aws.web.factory.S3ImplFactory;
import com.spinsys.util.Utils;
import com.spinsys.zerotrust.broker.security.CryptoKeys;
import com.spinsys.zerotrust.io.NodeBlock;
import com.spinsys.zerotrust.io.QueueRecordsList;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;
import com.spinsys.zerotrust.util.Constants.BrokerRequest;
import com.spinsys.zerotrust.util.Constants.BrokerType;
import com.spinsys.zerotrust.util.Constants.DataFormat;
import com.spinsys.zerotrust.util.Constants.S3Action;
import com.veetechis.lib.security.Crypter;
import com.veetechis.lib.security.CrypterException;
import com.veetechis.lib.security.Encoder;
import com.veetechis.lib.security.Crypter.CipherStrength;
import com.veetechis.lib.util.ArrayConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Simulates a Vitro Zero-trust IoT device for testing.
 * 
 * @author  acook@spinsys.com
 */
public class DeviceSim
{
	public static void main( String[] args )
	throws ConfigurationException
	{
		try {
			Parameters config = new Parameters( args );

			DeviceType deviceType;
			String clientType = System.getProperty( "deviceType" );
			if (clientType == null) {
				deviceType = DeviceType.BROKERCLIENT;
			}
			else {
				deviceType = DeviceType.valueOf( clientType.toUpperCase() );
			}

			if (deviceType == DeviceType.BROKERCLIENT && !(config.hasMessageBroker() || config.hasBrokerService())) {
				throw new ConfigurationException( "Broker not configured." );
			}

			if (deviceType == DeviceType.MQTTCLIENT) {
				monitorIncoming( config );
			}
			else {
				sendOutgoing( deviceType, config );
			}
		}
		catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( "Error running client.", e );
			}
			if (e instanceof ConfigurationException) {
				throw (ConfigurationException) e;
			}
		}
	}


	private static void monitorIncoming( Parameters config )
	throws Exception
	{
		if (!config.hasMessageBroker()) {
			throw new ConfigurationException( "Message broker not configured." );
		}

		System.out.println( String.format("Listening for incoming %s messages from broker %s on topic %s.",
			new Object[] {config.getQueueType(), config.getQueueServerAddress(), config.getQueueName()}) );
		System.out.println( "CTRL-C to quit..." );
			
		MQTTClient client = new MQTTClient( MQTTClient.ClientType.RECEIVER, config );
		// CTRL-C to quit...
	}

	private static void sendOutgoing( DeviceType clientType, Parameters config )
	throws Exception
	{
		// 1. Generate crypto set: signature key pair + symmetric key/iv
		//
		CryptoKeys keys = generateCryptoKeys();
		Utils.dumpData( keys );
		boolean secured = false;

		// 2. Generate IoT sensor messages ("node block")
		//
		String clientId = Encoder.generateKey( 12 );
		int maxBlocks = config.getOperationCount();
		ArrayList<NodeBlock> iotBlocks = new ArrayList<NodeBlock>();
		for ( int i = 0; i < maxBlocks; i++ ) {
			if (maxBlocks > 1) {
				if (config.doUploadToS3()) {
					config.setS3ObjectName( null );
				}
				try {
					Thread.sleep( 1000 );
				}
				catch (InterruptedException e) {
					// meh
					System.err.println( String.format("Operation interrupted. E = %s", e.getMessage()) );
				}
			}

			NodeBlock iotBlock = generateBlockMessage( keys, (maxBlocks > 1) );
			Utils.dumpData( iotBlock );
			iotBlocks.add( iotBlock );

			//
			// 3. Send public key + symmetric key to broker, if available...
			//
			if (config.hasBrokerService() && !secured) {
				secured = secureKeys( iotBlock.getHexID(), keys, config );
			}
		}

		String response = null;
		switch (clientType) {
			case BROKERCLIENT:
				// 4. Send sensor messages to queue (router), or directly to
				//    broker...
				response = sendToBroker( clientId, iotBlocks, config );
				break;
			case AWSCLIENT:
				// ...or, upload directly to storage.
				response = sendToAWS( clientId, iotBlocks, config );
				break;
			default:
				throw new ConfigurationException( "TODO: unsupported client type: " + clientType );
		}

		if (config.hasOutputPath()) {
			File outputFile = writeFile( response.getBytes(), getOutputFile(config) );
			System.out.println( String.format("Wrote response message to file %s.", outputFile.getName()) );
		}
	}

	private static boolean secureKeys( String requesterId, CryptoKeys keys, Parameters config )
	throws Exception
	{
		boolean secured;
		String keysResponse = null;

		if (config.getBrokerType().equals(BrokerType.HTTP)) {
			HttpBrokerClient client = new HttpBrokerClient( config );
			BrokerResponse brosponse = client.secureKeys( requesterId, keys );
			keysResponse = brosponse.getResponseBody();
			int status = brosponse.getStatusCode();
			secured = (status >= 200 && status < 300);
		}
		else {
			BrokerPayload payload = new BrokerPayload();
			payload.setCryptoKeys( keys );
			config.setBrokerRequestPayload( requesterId, payload );
			config.setBrokerRequestType( requesterId, BrokerRequest.SECURE_KEYS );
			keysResponse = LambdaImplFactory.getBrokerImplementation( requesterId, config ).execute();
			secured = (keysResponse.indexOf("secured") > -1);
		}

		System.out.println( String.format("Got response from secureKeys for requester %s: secured = %s\n'%s'",
			new Object[] {requesterId, secured, keysResponse}) );

		return secured;
	}

	private static String sendToBroker( String requesterId, List<NodeBlock> iotBlocks, Parameters config )
	throws Exception
	{
		StringBuilder blokResponse = new StringBuilder();

		if (config.doSendToRouter()) {
			// 4. Send sensor messages to queue (router), or...
			MQTTClient client = new MQTTClient( MQTTClient.ClientType.SENDER, config );
			client.connect();
			if (client.isConnected()) {
				String result = client.sendIotMessage( requesterId, iotBlocks );
				blokResponse.append( result );
			}
			client.disconnect();
		}
		else {
			// 4. Send sensor messages directly to broker for testing.
			ArrayList<String> nodeBlocks = new ArrayList<String>( iotBlocks.size() );
			for (NodeBlock iotBlock : iotBlocks ) {
				nodeBlocks.add( iotBlock.toString() );
			}
			if (config.getBrokerType().equals(BrokerType.HTTP)) {
				HttpBrokerClient client = new HttpBrokerClient( config );
				BrokerResponse brosponse;

				// test block validation
				brosponse = client.validateIot( nodeBlocks );
				blokResponse.append( String.format("Validate IoT:\n%s\n", brosponse.getResponseBody()) );

				// test block decryption
				brosponse = client.decryptIot( nodeBlocks );
				blokResponse.append( String.format("Decrypt IoT:\n%s", brosponse.getResponseBody()) );
			}
			else {
				BrokerPayload payload = new BrokerPayload();
				payload.setNodeData( nodeBlocks );
				config.setBrokerRequestPayload( requesterId, payload );

				// test block validation
				config.setBrokerRequestType( requesterId, BrokerRequest.VALIDATE_IOT );
				String response = LambdaImplFactory.getBrokerImplementation( requesterId, config ).execute();
				blokResponse.append( String.format("Validate IoT:\n%s\n", response) );

				// test block decryption
				config.setBrokerRequestType( requesterId, BrokerRequest.DECRYPT_IOT );
				response = LambdaImplFactory.getBrokerImplementation( requesterId, config ).execute();
				blokResponse.append( String.format("Decrypt IoT:\n%s", response) );
			}
		}

		return blokResponse.toString();
	}

	private static String sendToAWS( String clientId, List<NodeBlock> iotBlocks, Parameters config )
	throws Exception
	{
		StringBuilder awsResponse = new StringBuilder();

		if (config.doS3Operation()) {
			S3Action action = S3Action.valueOf( config.getAWSAction() );
			String response;
			int index = 1;
			for (NodeBlock iotBlock : iotBlocks) {
				switch (action) {
					case PUT:
						if (!config.hasOutputPath()) {
							config.setOutputPath( "./" );
						}
						File uploadFile = writeBlock( clientId, iotBlock, config );
						config.setUploadFile( uploadFile );
						validateNodeObjectName(iotBlock, index++, config);
						break;
					case SELECT:
						break;
					default:
						throw new RuntimeException( String.format("TODO: unsupported S3 action %s", action.name()) );
				}

				response = S3ImplFactory.getS3Implementation( action, config ).execute();
				awsResponse.append( String.format("%s\n", response) );
			}
		}

		return awsResponse.toString();
	}

	private static NodeBlock generateBlockMessage( CryptoKeys keys, boolean persistentNode )
	throws IOException, CrypterException
	{
		byte[] buff = new byte[NodeBlock.MINIMUM_LENGTH];
		int offset = 0;

		// 0. Parameter ID
		byte[] pidBytes = Encoder.generateKey(NodeBlock.PARAM_ID_SIZE).getBytes();
		Utils.copyData( buff, offset, pidBytes, NodeBlock.PARAM_ID_SIZE );
		offset += NodeBlock.PARAM_ID_SIZE;

		// 1. ECC ID [NodeID/DeviceID]
		String nodeId;
		if ( persistentNode ) {
			if ( persistentNodeId == null ) {
				persistentNodeId = Encoder.generateKey( NodeBlock.ECC_ID_SIZE );
			}
			nodeId = persistentNodeId;
		}
		else {
			nodeId = Encoder.generateKey( NodeBlock.ECC_ID_SIZE );
		}
		byte[] nodeIdBytes = nodeId.getBytes();
		Utils.copyData( buff, offset, nodeIdBytes, NodeBlock.ECC_ID_SIZE );
		offset += NodeBlock.ECC_ID_SIZE;

		// 2. Timestamp
		byte[] tstampBytes = ArrayConverter.intToBytes( (int) Instant.now().getEpochSecond() );
		Utils.copyData( buff, offset, tstampBytes, NodeBlock.TIMESTAMP_SIZE );
		offset += NodeBlock.TIMESTAMP_SIZE;

		// 3. Initialization Vector (IV)
		byte[] ivBytes = keys.initVector.getIV();
		Utils.copyData( buff, offset, ivBytes, NodeBlock.IV_SIZE );
		offset += NodeBlock.IV_SIZE;

		// 4. Data
		String data = Encoder.generateKey( NodeBlock.SENSOR_DATA_SIZE );
		LOG.info( String.format("Generated sensor data for node %s = %s", nodeId, data) );

		byte[] dataBytes = Crypter.encrypt( CipherStrength.AES_CBC_PKCS5, data.getBytes(), keys.symmetricKey, keys.initVector );
		Utils.copyData( buff, offset, dataBytes, NodeBlock.ENCRYPTED_DATA_SIZE );
		offset += NodeBlock.ENCRYPTED_DATA_SIZE;

		// 5. Digest/Hash
		byte[] hashBytes = buildDigestable( ivBytes, dataBytes, pidBytes, nodeIdBytes, tstampBytes );
		byte[] hash = Crypter.digest( CipherStrength.SHA_MEDIUM, hashBytes );
		Utils.copyData( buff, offset, hash, NodeBlock.HASH_SIZE );
		offset += NodeBlock.HASH_SIZE;

		// 6. Signature
		byte [] signature = Crypter.sign( CipherStrength.SHA_ECDSA_MEDIUM, hash, keys.privateKey );
		Utils.copyData( buff, offset, signature, NodeBlock.SIGNATURE_SIZE );

		return NodeBlock.getInstance( buff );
	}

	private static byte[] buildDigestable( byte[] iv, byte[] data, byte[] pid, byte[] nodeId, byte[] tstamp )
	{
		// Per Vitro's documentation...
		// "Data which is hashed is Initialization Vector (16 bytes), encrypted
		// data (16 bytes[+]), Parameter ID (2 bytes), Node ID (9 bytes) and
		// timestamp (4 bytes). It is collected into structure which has exactly
		// this order..."

		byte[] buff = new byte[NodeBlock.DIGESTABLE_LENGTH + (data.length - NodeBlock.ENCRYPTED_DATA_SIZE)];
		int offset = 0;

		// IV
		Utils.copyData( buff, offset, iv, NodeBlock.IV_SIZE );
		offset += NodeBlock.IV_SIZE;

		// Encrypted data
		Utils.copyData( buff, offset, data, data.length );
		offset += data.length;

		// Parameter ID
		Utils.copyData( buff, offset, pid, NodeBlock.PARAM_ID_SIZE );
		offset += NodeBlock.PARAM_ID_SIZE;

		// Node ID
		Utils.copyData( buff, offset, nodeId, NodeBlock.ECC_ID_SIZE );
		offset += NodeBlock.ECC_ID_SIZE;

		// Timestamp
		Utils.copyData( buff, offset, tstamp, NodeBlock.TIMESTAMP_SIZE );

		return buff;
	}

	private static CryptoKeys generateCryptoKeys()
	throws CrypterException
	{
		KeyPair keyPair = Crypter.generateSignatureKeys( CipherStrength.ECDSA_WEAK );
		SecretKey secret = Crypter.generateSymmetricKey( CipherStrength.AES_STRONG );
		IvParameterSpec iv = Crypter.generateIv();

		return new CryptoKeys( keyPair.getPrivate(), keyPair.getPublic(), secret, iv );
	}

	private static void validateNodeObjectName(NodeBlock iotBlock, int index, Parameters config)
	{
		String objectName = config.getS3ObjectName();
		if (objectName == null) {
			objectName = "crystal1";
		}
		if (objectName.split("\\/").length == 1) {
			int itimestamp = iotBlock.getIntegerTimestamp();
			Calendar cal = Calendar.getInstance();
			cal.setTime( Date.from(Instant.ofEpochSecond(itimestamp)) );
			String year = String.format( "%4d", cal.get(Calendar.YEAR) );
			String month = String.format( "%2d", cal.get(Calendar.MONTH) + 1 );
			String day = String.format( "%2d", cal.get(Calendar.DATE) );
			objectName = String.format( "%s/%s/year=%s/month=%s%s/day=%s%s%s/%s%s",
				new Object[] {objectName, String.format("node%s", iotBlock.getHexID()),
					year, year, month, year, month, day,
					String.format("%d-%d", itimestamp, index), config.getOutputFormat().getSuffix()} );
			config.setS3ObjectName( objectName );
		}
	}

	/*
	 * Writes the give response to one of the following:
	 * <ul>
	 * <li>The output file path specified in parameters if not a directory; or</li>
	 * <li>A file in the output directory specified in parameters with file name
	 * pattern: <deviceId>-<timestamp>.json.</li>
	 * </ul>
	 */
	private static File writeFile( byte[] content, File outFile )
	{
		try (FileOutputStream out = new FileOutputStream(outFile)) {
			out.write( content );
			out.flush();
			System.out.println( String.format("Content written to file %s.", outFile.toString()) );
		}
		catch( Exception e ) {
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error creating output file %s.", outFile), e );
			}
			outFile = null;
		}

		return outFile;
	}

	/*
	 * Writes the given IoT block to one of the following:
	 * <ul>
	 * <li>The output file path specified in parameters if not a directory; or</li>
	 * <li>A file in the output directory specified in parameters with file name
	 * pattern: <deviceId>-<timestamp>.json.</li>
	 * </ul>
	 */
	private static File writeBlock( String clientId, NodeBlock iotBlock, Parameters config )
	{
		File dirFile = new File( config.getOutputPath() );
		if (!dirFile.isDirectory()) {
			dirFile = dirFile.getParentFile();
		}
		DataFormat fmt = config.getOutputFormat();
		String baseName = new String(iotBlock.getID());
		String timestamp = Constants.TIMESTAMP_FORMAT.format( Calendar.getInstance().getTime() );
		File outFile = new File( dirFile, String.format("%s-%s%s", baseName, timestamp, fmt.getSuffix()) );

		byte[] content;
		if (config.doQueueOperation()) {
			QueueRecordsList recs = new QueueRecordsList();
			recs.add( clientId, iotBlock );
			content = Utils.beautify( recs.toString() ).getBytes();
		}
		else {
			if (fmt == DataFormat.AVRO) {
				content = iotBlock.toEventObject( clientId, fmt );
			}
			else {
				content = Utils.beautify( new String(iotBlock.toEventObject(clientId, fmt)) ).getBytes();
			}
		}

		return writeFile( content, outFile );
	}

	private static void uploadFile( File content, Parameters config, boolean deleteOnUpload )
	{
		if (config.getS3ObjectName() == null) {
			config.setS3ObjectName( content.getName() );
		}
		config.setUploadFile( content );
		S3Impl s3 = S3ImplFactory.getS3Implementation( S3Action.PUT, config );
		String result = s3.execute();
		System.out.println( String.format("Received S3 action result:\n%s", Utils.beautify(result)) );

		if (deleteOnUpload) {
			content.deleteOnExit();
		}
	}

	public static File getOutputFile( Parameters config )
	{
		File responseFile = new File( config.getOutputPath() );

		File outFile;
		if (responseFile.isDirectory()) {
			String baseName = "BrokerResult";
			String timestamp = Constants.TIMESTAMP_FORMAT.format( Calendar.getInstance().getTime() );
			if (config.doS3Operation()) {
				baseName = String.format( "%sResult", Utils.toCamelCase(config.getAWSAction()) );
			}
			outFile = new File( responseFile, String.format("%s-%s.json", baseName, timestamp) );
		}
		else {
			outFile = responseFile;
		}

		return outFile;
	}


	private static enum DeviceType {
		AWSCLIENT( "AWS client" ),
		BROKERCLIENT( "Vitro broker client" ),
		MQTTCLIENT( "MQTT client" );

		public String toString()
		{
			return description;
		}

		DeviceType( String description )
		{
			this.description = description;
		}

		private final String description;
	}

	private static String persistentNodeId = null;

	private static final Log LOG = LogFactory.getLog( DeviceSim.class );
}
