/*
 * HttpBrokerClient
 * 
 * Sep 9, 2021
 * 
 * Change Activity Log
 * Reason  Date     Who  Description
 * ------  -------- ---  -----------
 */
package com.spinsys.zerotrust.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;

import com.spinsys.zerotrust.broker.security.CryptoKeys;
import com.spinsys.zerotrust.util.Constants;
import com.spinsys.zerotrust.util.Parameters;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;


/**
 * HttpBrokerClient provides REST access to the MDACA Vitro Broker decryption
 * service.
 */
public class HttpBrokerClient
implements Constants
{
	protected static final String HTTP_STATUS_CODE_KEY = "statusCode";
	

	/**
	 * Creates a new HTTP client instance with the given configuration
	 * parameters.
	 * 
	 * @param config  the configuration parameters.
	 * @throws java.net.MalformedURLException
	 */
	public HttpBrokerClient( Parameters config )
	throws MalformedURLException
	{
		this.config = config;

		String hostUrl = config.getBrokerAddress();
		while (hostUrl.endsWith("/")) {
			hostUrl = hostUrl.substring( 0, hostUrl.length()-1 );
		}

		brokerUrl = new URL( hostUrl );
	}

	
	/**
	 * Sends the given node's crypto keys to the broker for storage.
	 * 
	 * @param requesterId  the requester's (node/device) identifier.
	 * @param keys  the crypto keys to store.
	 * @return 
	 * @throws IOException
	 */
	public synchronized BrokerResponse secureKeys( String requesterId, CryptoKeys keys )
	throws BrokerException
	{
		BrokerResponse response;
		try {
			String requestBody = createSecureKeysRequestBody( requesterId, keys );
			HashMap<String,String> requestHeaders = getBaseRequestHeaders();
			HashMap<String,String> responseHeaders = new HashMap<>();
	
			String responseBody = callService( BrokerRequest.SECURE_KEYS, requestBody, requestHeaders, responseHeaders );
			response = new BrokerResponse( Integer.valueOf(responseHeaders.get(HTTP_STATUS_CODE_KEY)), responseBody );
		}
		catch (Exception e) {
			throw new BrokerException( String.format("Error while calling broker to secure crypto keys for requester %s.", requesterId), e );
		}

		return response;
	}

	/**
	 * Sends the given list of node blocks to the broker for validation.
	 * 
	 * @param nodeBlocks  the list of node blocks to validate.
	 * @return 
	 * @throws IOException
	 */
	public synchronized BrokerResponse validateIot( List<String> nodeBlocks )
	throws BrokerException
	{
		BrokerResponse response;
		try {
			String requestBody = createNodeBlockListRequestBody( nodeBlocks );
			HashMap<String,String> requestHeaders = getBaseRequestHeaders();
			HashMap<String,String> responseHeaders = new HashMap<>();
	
			String responseBody = callService( BrokerRequest.VALIDATE_IOT, requestBody, requestHeaders, responseHeaders );
			response = new BrokerResponse( Integer.valueOf(responseHeaders.get(HTTP_STATUS_CODE_KEY)), responseBody );
		}
		catch (Exception e) {
			throw new BrokerException( String.format("Error while calling broker to validate %d node blocks.", nodeBlocks.size()), e );
		}

		return response;
	}

	/**
	 * Sends the given list of node blocks to the broker for decryption.
	 * 
	 * @param nodeBlocks  the list of node blocks to decrypt.
	 * @return 
	 * @throws IOException
	 */
	public synchronized BrokerResponse decryptIot( List<String> nodeBlocks )
	throws BrokerException
	{
		BrokerResponse response;
		try {
			String requestBody = createNodeBlockListRequestBody( nodeBlocks );
			HashMap<String,String> requestHeaders = getBaseRequestHeaders();
			HashMap<String,String> responseHeaders = new HashMap<>();
	
			String responseBody = callService( BrokerRequest.DECRYPT_IOT, requestBody, requestHeaders, responseHeaders );
			response = new BrokerResponse( Integer.valueOf(responseHeaders.get(HTTP_STATUS_CODE_KEY)), responseBody );
		}
		catch (Exception e) {
			throw new BrokerException( String.format("Error while calling broker to decrypt %d node blocks.", nodeBlocks.size()), e );
		}

		return response;
	}


	/**
	 * Sends the given request body to the service endpoint specified by the
	 * broker request type.
	 * 
	 * @param requestType
	 * @param requestHeaders
	 * @param requestBody
	 * @param responseHeaders
	 * @return
	 * @throws Exception 
	 */
	protected String callService( BrokerRequest requestType, String requestBody,
			HashMap<String,String> requestHeaders, HashMap<String,String> responseHeaders )
	throws Exception
	{
		String response = "";
		
		HttpRequestBase request = createHttpRequest( requestType, requestBody, getHttpParameters() );
		requestHeaders.keySet().forEach(header -> {
			request.setHeader( header, requestHeaders.get(header) );
		});
		
		if( LOG.isInfoEnabled() ) {
			Header[] headers = request.getAllHeaders();
			StringBuilder buff = new StringBuilder( "[" );
			for( int i = 0; i < headers.length; i++ ) {
				if( i > 0 ) buff.append( ", " );
				buff.append( headers[i].getName() ).append( ": " ).append( headers[i].getValue() );
			}
			buff.append( "]" );
			String requestLine = request.getRequestLine().toString();
			LOG.info( String.format("Sending request to vault broker:\nRequest line = %s\nRequest headers = %s\nRequest entity = %s",
					new Object[] {requestLine, buff.toString(), requestBody}) );
		}
		
		long startTime = System.currentTimeMillis();
		HttpResponse resp = getHttpClient().execute( request );
		
		if( LOG.isTraceEnabled() ) {
			LOG.info( String.format("Elapsed time to call vault broker: %s", String.valueOf(System.currentTimeMillis() - startTime)) );
		}
		
		int statusCode = resp.getStatusLine().getStatusCode();
		responseHeaders.put( HTTP_STATUS_CODE_KEY, String.valueOf(statusCode) );
		
		if( LOG.isDebugEnabled() ) {
			LOG.debug( String.format("Received status code: %s for endpoint %s.", statusCode, requestType.getRequestPath()) );
		}
		
		HttpEntity entity = resp.getEntity();
		if( entity != null ) {
			response = EntityUtils.toString( entity );
		}

		if( LOG.isInfoEnabled() ) {
			LOG.info( String.format("Received response from endpoint %s:\n%s", requestType.getRequestPath(), response) );
		}
		
		for (Header header : resp.getAllHeaders()) {
			responseHeaders.put( header.getName(), header.getValue() );
		}
		
		return response;
	}

	protected HttpRequestBase createHttpRequest( BrokerRequest brokerRequest, String body, HttpParams parameters )
	throws Exception
	{
		HttpRequestBase request;
		if (brokerRequest.getRequestMethod().equals("PUT")) {
			request = new HttpPut();
			((HttpPut) request).setEntity( new StringEntity(body) );
		}
		else {
			request = new HttpPost();
			((HttpPost) request).setEntity( new StringEntity(body) );
		}
		request.setParams( parameters );
		request.setURI( new URI(String.format("%s%s", brokerUrl.toString(), brokerRequest.getRequestPath())) );

		return request;
	}
	
	protected CloseableHttpClient getHttpClient()
	{
		if (httpClient == null) {
			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext;
			try {
				sslContext = SSLContexts.custom().loadTrustMaterial( null, acceptingTrustStrategy ).build();
				SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory( sslContext );

				Registry<ConnectionSocketFactory> socketFactoryRegistry =
						RegistryBuilder.<ConnectionSocketFactory> create()
								.register( "https", sslsf )
								.register( "http", new PlainConnectionSocketFactory() )
								.build();

				BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager( socketFactoryRegistry );
				httpClient = HttpClients.custom().
						disableCookieManagement().
						setSSLSocketFactory( sslsf ).
						setConnectionManager( connectionManager ).
						build();
			}
			catch ( NoSuchAlgorithmException | KeyStoreException | KeyManagementException e ) {
				if (LOG.isErrorEnabled()) {
					LOG.error( "Error creating HTTP client.", e );
				}
			}
		}
		
		return httpClient;
	}
	
	protected HttpParams getHttpParameters()
	{
		BasicHttpParams params = new BasicHttpParams();
		params.setIntParameter( CoreConnectionPNames.SO_TIMEOUT, 30000 );
		params.setIntParameter( CoreConnectionPNames.CONNECTION_TIMEOUT, 10000 );
		
		return params;
	}

	protected String createSecureKeysRequestBody( String nodeId, CryptoKeys keys )
	throws IOException
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.addStringField( "nodeId", nodeId );
		writer.addStringField( "encryptionKey", Hex.encodeHexString(keys.symmetricKey.getEncoded()) );
		writer.addStringField( "signatureKey", Hex.encodeHexString(keys.publicKey.getEncoded()) );
		writer.endDocument();
	
		return writer.toString();
	}

	protected String createNodeBlockListRequestBody( List<String> nodeBlocks )
	throws IOException
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		writer.startList( "nodeBlocks" );
		for (String nodeBlock: nodeBlocks) {
			writer.addElement( "\"" + nodeBlock + "\"" );
		}
		writer.endList();
		writer.endDocument();
	
		return writer.toString();
	}

	protected HashMap<String,String> getBaseRequestHeaders()
	{
		HashMap<String, String> headers = new HashMap<>();
		headers.put( HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE );
		headers.put( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE );
		headers.put( HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.toString() );

		String authToken = config.getBrokerAuthenticationToken();
		if (authToken != null) {
			headers.put( HttpHeaders.WWW_AUTHENTICATE, String.format("Negotiate %s", authToken) );
		}

		return headers;
	}


	private CloseableHttpClient httpClient;
	
	private final Parameters config;
	private final URL brokerUrl;

	private static final Log LOG = LogFactory.getLog( HttpBrokerClient.class );
}
