package com.spinsys.zerotrust.client;

public class BrokerResponse
{
	public BrokerResponse( Integer statusCode, String responseBody )
	{
		this.statusCode = statusCode;
		this.responseBody = responseBody;
	}

	public Integer getStatusCode()
	{
		return statusCode;
	}

	public String getResponseBody()
	{
		return responseBody;
	}


	private final String responseBody;
	private final Integer statusCode;
}
