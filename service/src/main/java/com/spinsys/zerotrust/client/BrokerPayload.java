package com.spinsys.zerotrust.client;

import java.util.ArrayList;
import java.util.List;

import com.spinsys.zerotrust.broker.db.Customer;
import com.spinsys.zerotrust.broker.security.CryptoKeys;
import com.spinsys.zerotrust.io.NodeBlock;

public class BrokerPayload
{
	public void setCryptoKeys( CryptoKeys keys )
	{
		cryptoKeys = keys;
	}

	public CryptoKeys getCryptoKeys()
	{
		return cryptoKeys;
	}

	public void setCustomerInfo( Customer info )
	{
		customerInfo = info;
	}

	public Customer getCustomerInfo()
	{
		return customerInfo;
	}

	public void setNodeData( List<String> data )
	{
		nodeData = data;
	}

	public List<String> getNodeData()
	{
		if (nodeData == null) {
			nodeData = new ArrayList<String>();
		}

		return nodeData;
	}


	private CryptoKeys cryptoKeys;
	private Customer customerInfo;
	private List<String> nodeData;
}
